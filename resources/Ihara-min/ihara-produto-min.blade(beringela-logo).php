@extends('layout')
@section('title','Produto')
@section('content')


 <!--Page Title-->
        <section class="page-title page-title-prod-min" style="height:80vh !important; background-image:url(images/background/template-site.jpg">
            <div class="col-6"></div>
            <div class="col-6 page-title-prod-min-col">
                <div class="page-title-prod-min-col-img">
                    <img src="{{asset('images/gallery/tuval.png')}}" alt="" srcset="">
                </div>
            </div>
        </section>
        <!--End Page Title-->

        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <!--Market Single-->
                        <div class="market-single">
                            <div class="inner-box">
                                <div class="image">
                                    
                                </div>
                                <div class="lower-content">
                                    <h2 id="title-eco-shot">Tuval</h2>
                                    <ul class="page-breadcrumb">
                                        <li>Prod. Especiais</li>
                                        <li class="breadcrumb-eco-shot">Tuval</li>
                                    </ul>

                                    <section class="section-indicacoes">
                                            <div class="container col-12">
                                                <h3 class="title text-center section-indicacoes-title">Confira as indicações de cultura</h3>
                                                <div class="container col-10 section-indicacoes-container-select">
                                                    <div class="indicacoes-box" style="width: 290px !important">
                                                        <select class="" name="" id="" style="background: white; color:black">
                                                            <option value="">Algodão</option>
                                                        </select>
                                                        {{-- <button class="btn btn-danger btn-arrow">▼</button> --}}
                                                    </div>
                                                </div>
                                            </div>
                        
                        
                                        </section>


                                </div>
                            </div>
                        </div>
                    </div>


                    <!--Sidebar Side-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <aside class="sidebar market-sidebar sidebar-prod-min">

                            <div class="sidebar-title">
                                <h2>FICHA TÉCNICA</h2>
                            </div>
                            <!--Blog Category Widget-->
                            <table class="table table-ficha-tecnica">
                                <tbody>
                                    <tr>
                                        <td class="table-cel-title">Ingrediente Ativo</td>
                                        <td>Cloreto de Clormequate</td>
                                    </tr>
                                    <tr>
                                        <td class="table-cel-title">Classe Ambiental</td>
                                        <td>III</td>
                                    </tr>
                                    <tr>
                                        <td class="table-cel-title">Concentração</td>
                                        <td>100,0 g/L</td>
                                    </tr>
                                    <tr>
                                        <td class="table-cel-title">Tipo de Formulação</td>
                                        <td>Concentrado Solúvel (SL)</td>
                                    </tr>
                                    <tr>
                                        <td class="table-cel-title">Classe Toxicológica</td>
                                        <td>IV</td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- Sidebar brochure-->

                        </aside>
                    </div>
                {{-- END SIDEBAR SIDE --}}
                </div>
            </div>

                {{-- CONTENT AFTER SIDEBAR STYLE --}}


 





                <div class="noticias">
                        <h3 class="title noticias-title text-center">Veja também</h3>
                    <div class="container-noticias">
                        <div class="col-lg-3">
                            <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                                    class="noticias-link"
                                    href="http://www.ihara.com.br/noticias/noticias-de-mercado/mt-aumenta-a-preocupacao-com-armazenamento/4278">MT:
                                    aumenta a preocupação com armazenamento</a> </h4>
                            <p>A produção de milho no Estado vem ganhando força e já alcançou 31,08 milhões de
                                toneladas este ano... </p>
                        </div>
                        <div class="col-lg-3">
                            <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                                    class="noticias-link"
                                    href="http://www.ihara.com.br/noticias/ihara-na-midia/proximidade-do-periodo-umido-aumenta-preocupacao-com-o-greening-no/4277">Proximidade
                                    do período úmido aumenta preocupação com o greening no campo</a> </h4>
                            <p>Afetando mais de 37 milhões de laranjeiras do cinturão citrícola de São Paulo e
                                Triângulo/Sudoeste Mineiro...</p>

                        </div>
                        <div class="col-lg-3">
                            <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                                    class="noticias-link"
                                    href="http://www.ihara.com.br/noticias/noticias-de-mercado/aumento-do-dolar-fez-esmagadoras-comprarem-mais-soja/4276">
                                    Aumento do dólar fez esmagadoras comprarem mais soja</a></h4>
                            <p>As indústrias esmagadoras compraram a maior parte das 700 mil toneladas
                                negociadas ontem, devido ao aumento...</p>
                        </div>
                    </div>
                </div>





            </div>
        </div>


          @endsection