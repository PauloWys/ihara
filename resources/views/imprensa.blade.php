@extends('layout')
@section('title','Imprensa')
@section('content')

<!--Page Title-->
<section class="page-title div-susten" style="background-image:url('images/images-sustentabilidade.jpg')">
    <div class="auto-container">
        <ul class="page-breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Imprensa</li>
        </ul>
        <h1>Imprensa</h1>

    </div>
</section>
<h1 style=" position: relative;left: 40%;bottom: 120px;z-index: 99999;color: white;font-size: 40px;">A Ihara na mídia</h1>
<!--End Page Title-->

<!--Sidebar Page Container-->
<div class="sidebar-page-container" style="padding: 60px 0px;">
    {{-- <div class="auto-container"> --}}
        {{-- <div class="row clearfix"> --}}


            <section class="wow fadeIn hover-option4" style="visibility: visible; animation-name: fadeIn;font-size: 2em">

                <div class="container">
                    <div class="row">

                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" style=" margin-top: 60px; visibility: visible; animation-name: fadeInLeft; height: 543px;">
                            <div class="inner-match-height">
                                <div class=" overflow-hidden position-relative pt-3 pb-3">
                                    <a href="https://exame.abril.com.br/negocios/ihara-tem-lucro-de-r-75-96-milhoes-em-2011-2/">
                                        <img src="https://www.agenciawys.com.br/assets/images/imprensa/imprensa-revista-exame.jpg" alt="" data-no-retina="">
                                    </a>
                                </div>
                                <div class="post-details  pt-3 pb-3">
                                    <a class="text-ihara" href="https://exame.abril.com.br/negocios/ihara-tem-lucro-de-r-75-96-milhoes-em-2011-2/" class="w-100 d-block mb-5">
                                        Ihara tem lucro de R$ 75,96 milhões em 2011
                                    </a>
                                    <p class="mt-3">
                                        Em 2011, a receita da empresa ficou em R$ 647,1 milhões, alta de 29,87% sobre o faturamento de R$ 498,27 milhões do período anterior.
                                    </p>
                                    <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                    <div class="author">
                                        <span class="text-uppercase text-extra-small display-inline-block sm-display-block sm-margin-10px-top">
                                            <a href="https://exame.abril.com.br/negocios/ihara-tem-lucro-de-r-75-96-milhoes-em-2011-2/" class="btn btn-rounded btn-transparent-dark-gray" data-wow-delay="0.4s">
                                                Ler Matéria
                                                <i class="ti-arrow-right" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 wow fadeInUp" style=" margin-top: 60px; visibility: visible; animation-name: fadeInUp; height: 543px;">
                            <div class="inner-match-height">
                                <div class=" overflow-hidden position-relative pt-3 pb-3">
                                    <a href="https://www.agrolink.com.br/noticias/ihara-celebra-50-anos-e-comemora-crescimento-no-pais_217085.html" target="_blank">
                                          <img style="height: 247px;" src="http://localhost/ihara/public/images/imprensa/agro.png" alt="" data-no-retina="">
                                    </a>
                                </div>
                                <div class="post-details  pt-3 pb-3">
                                    <a class="text-ihara" href="https://www.agrolink.com.br/noticias/ihara-celebra-50-anos-e-comemora-crescimento-no-pais_217085.html" target="_blank" class="w-100 d-block mb-5">
                                        IHARA celebra 50 anos e comemora crescimento no país    
                                    </a>
                                    <p class="mt-3">
                                        A IHARA celebrou na noite desta quarta-feira (18) o seu 50º aniversário. Para comemorar a ocasião, a tradicional...

                                    </p>
                                    <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                    <div class="author">
                                        <span class="text-uppercase text-extra-small display-inline-block sm-display-block sm-margin-10px-top">
                                            <a href="https://www.agrolink.com.br/noticias/ihara-celebra-50-anos-e-comemora-crescimento-no-pais_217085.html" target="_blank" class="btn btn-rounded btn-transparent-dark-gray" data-wow-delay="0.4s">
                                                Ler Matéria
                                                <i class="ti-arrow-right" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid-item col-md-4 col-sm-6 col-xs-12 wow fadeInRight" style="margin-top: 60px; visibility: visible; animation-name: fadeInRight; height: 543px;">
                            <div class="inner-match-height">
                                <div class=" overflow-hidden position-relative pt-3 pb-3">
                                    <a href="http://www.abccriadores.com.br/NoticiasTexto.aspx?idNoticia=3215">
                                        <img src="http://localhost/ihara/public/images/imprensa/logoabc.jpg" alt="" data-no-retina="">
                                    </a>
                                </div>
                                <div class="post-details  pt-3 pb-3">
                                    <a class="text-ihara" href="http://www.abccriadores.com.br/NoticiasTexto.aspx?idNoticia=3215" class="w-100 d-block mb-5">
                                        Nova molécula fortalece negócios da Ihara
                                    </a>
                                    <p class="mt-3">
                                        A Ihara, fabricante brasileira de agrotóxicos controlada por sete empresas japonesas, comemora, depois de quase dez anos de espera e ...

                                  </p>
                                  <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                  <div class="author">
                                    <span class="text-uppercase text-extra-small display-inline-block sm-display-block sm-margin-10px-top">
                                        <a href="http://www.abccriadores.com.br/NoticiasTexto.aspx?idNoticia=3215" class="btn btn-rounded btn-transparent-dark-gray" data-wow-delay="0.4s">
                                            Ler Matéria
                                            <i class="ti-arrow-right" aria-hidden="true"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-md-4 col-sm-6 col-xs-12 wow fadeInRight" style=" margin-top: 60px;    visibility: visible; animation-name: fadeInRight; height: 543px;">
                        <div class="inner-match-height">
                            <div class=" overflow-hidden position-relative pt-3 pb-3">
                                <a href="https://revistagloborural.globo.com/Noticias/Empresas-e-Negocios/noticia/2019/10/ihara-anuncia-registro-de-nova-molecula-no-brasil.html">
                                    <img src="http://localhost/ihara/public/images/imprensa/G1_logo.svg.png" alt="" data-no-retina="">
                                </a>
                            </div>
                            <div class="post-details  pt-3 pb-3">
                                <a class="text-ihara" href="https://revistagloborural.globo.com/Noticias/Empresas-e-Negocios/noticia/2019/10/ihara-anuncia-registro-de-nova-molecula-no-brasil.html" class="w-100 d-block mb-5">
                                    Ihara anuncia registro de nova molécula no Brasil
                                </a>
                                <p class="mt-3">
                                    Dinotefuran será base de linha de produtos voltados para controle de pragas como o percevejo na soja e a cigarrinha da cana-de-açúcar   
                                </p>
                                <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                <div class="author">
                                    <span class="text-uppercase text-extra-small display-inline-block sm-display-block sm-margin-10px-top">
                                        <a target="_blank" href="https://revistagloborural.globo.com/Noticias/Empresas-e-Negocios/noticia/2019/10/ihara-anuncia-registro-de-nova-molecula-no-brasil.html" class="btn btn-rounded btn-transparent-dark-gray" data-wow-delay="0.4s">
                                            Ler Matéria
                                            <i class="ti-arrow-right" aria-hidden="true"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-md-4 col-sm-6 col-xs-12 wow fadeInRight" style=" margin-top: 60px;    visibility: visible; animation-name: fadeInRight; height: 543px;">
                        <div class="inner-match-height">
                            <div class=" overflow-hidden position-relative pt-3 pb-3">
                                <a href="https://www.grupocultivar.com.br/noticias/ihara-lanca-molecula-inseticida-inedita-no-brasil">
                                    <img style=" padding: 45px;" src="http://localhost/ihara/public/images/imprensa/cultivar.png" alt="" data-no-retina="">
                                </a>
                            </div>
                            <div class="post-details  pt-3 pb-3">
                                <a class="text-ihara" href="https://www.terra.com.br/noticias/dino/neuromarketing-e-as-estrategias-que-influenciam-suas-demandas,5730b527f560ddc159ae615be694c15ckww4n77d.html" class="w-100 d-block mb-5">
                                    hara lança molécula inseticida inédita no Brasil
                                </a>
                                <p class="mt-3">
                                 Pesquisador fala sobre percevejo em soja; Ihara traz ao Brasil a molécula inseticida Dinotefuran, inédita no combate às principais pragas da agricultura.  
                             </p>
                             <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                             <div class="author">
                                <span class="text-uppercase text-extra-small display-inline-block sm-display-block sm-margin-10px-top">
                                    <a target="_blank" href="https://www.grupocultivar.com.br/noticias/ihara-lanca-molecula-inseticida-inedita-no-brasil" class="btn btn-rounded btn-transparent-dark-gray" data-wow-delay="0.4s">
                                        Ler Matéria
                                        <i class="ti-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="services-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{asset('images/resource/service-1.jpg')}}" alt="" />
                        </div>
                        <div class="lower-content">
                            <div class="icon-box">
                                <img style="height: 50px" src="{{ asset('images/background/fusao-logo-icon.png')}}" alt="fusão logo">
                            </div>

                            <h3>Texto Aqui</h3>
                        </div>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="text">There anyone who loves or pursues or desires too obtains pain of itself, because it is  workers all occur.</div>
                                <a href="/home" class="know-more">SAIBA MAIS <span class="icon flaticon-right-arrow-1"></span></a>
                                <div class="overlay-lower-inner">
                                    <div class="icon-box sem-cor mb-5">
                                        <img style="height: 50px" src="{{ asset('images/background/fusao-logo-icon.png')}}" alt="fusão logo">
                                    </div>
                                    <h4><a href="/home">Texto Aqui</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}

            </div>
        </div>
    </section>


{{-- </div> --}}
{{-- </div> --}}
</div>

@endsection