@extends('layout')
@section('title','Produto')
@section('content')


 <!--Page Title-->
        <section class="" style="background-image:url(images/34.jpg); background-position: bottom; background-size: cover;">
            <div class="container" style="display: flex; justify-content: center; align-items: center; height: 100%;">
                <h1 class="title text-center" style="color: white; text-transform: uppercase;">Encontre sua solução</h1>
            </div>
        </section>
        <!--End Page Title-->




        <section class="section-filter">
            <div class="container">
                    <div class="accordion" id="accordionExample">
                            <div class="card">
                              <div class="card-header section-filter-buscar-header" id="headingOne">
                                <h2 class="mb-0 section-filter-buscar-header-title">
                                  <button class="btn btn-link section-filter-buscar-header-title-btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Buscar produtos
                                  </button>
                                </h2>
                              </div>
                          
                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body section-filter-first-card-body">
                                  
                                    <div class="search-box">
                                        <input class="search-box-input" type="text" name="" placeholder="Digite a cultura, alvo ou o nome do produto">
                                        <a class="search-box-btn" href="">
                                            <button class="btn btn-dark btn-lg btn-search">Buscar</button>
                                        </a>
                                    </div>

                                    
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-header section-filter-buscar-header" id="headingTwo">
                                <h2 class="mb-0" section-filter-buscar-header-title>
                                  <button class="btn btn-link collapsed section-filter-buscar-header-title-btn" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Busca de produtos avançada
                                  </button>
                                </h2>
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body section-filter-second-card-body">
                                  
                                    <div class="row section-filter-second-card-body-first-row">
                                        <h2 class="section-filter-second-card-body-first-row-title">Selecione um Segmento :</h2>
                                        <div class="col-12 section-filter-advanced-container">
                                                <button class="btn btn-outline-danger btn-segment">Acaricidas</button>
                                                <button class="btn btn-outline-danger btn-segment">Biológicos</button>
                                                <button class="btn btn-outline-danger btn-segment">Fungicidas</button>
                                                <button class="btn btn-outline-danger btn-segment">Herbicidas</button>
                                                <button class="btn btn-outline-danger btn-segment">Inseticidas</button>
                                                <button class="btn btn-outline-danger btn-segment">Prod. Especiais</button>
                                        </div>
                                    </div>

                                    <div class="container section-filter-second-card-body-second-row">
                                        <div class="row">
                                            <div class="col-4">
                                                <h2 class="section-filter-second-card-body-first-row-title">Selecione uma cultura</h2>
                                                <div class="box-select">
                                                        <select name="" id="">
                                                                <option value=""></option>
                                                                <option value="">Abóbora</option>
                                                                <option value="">Abobrinha</option>
                                                                <option value="">Alface</option>
                                                                <option value="">Alho</option>
                                                                <option value="">Batata</option>
                                                                <option value="">Cebola</option>
                                                                <option value="">Cenoura</option>
                                                                <option value="">Maçã</option>
                                                                <option value="">Melancia</option>
                                                                <option value="">Melão</option>
                                                                <option value="">Morango</option>
                                                                <option value="">Ornamentais</option>
                                                                <option value="">Pepino</option>
                                                                <option value="">Tomate</option>
                                                                <option value="">Uva</option>
                                                            </select>
                                                            <button class="btn btn-danger btn-arrow">▼</button>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <h2 class="section-filter-second-card-body-first-row-title">Selecione um alvo</h2>
                                                <div class="box-select">
                                                        <select name="" id="">
                                                                <option value=""></option>
                                                                <option value="">Abobrinha</option>
                                                                <option value="">Alface</option>
                                                                <option value="">Alho</option>
                                                                <option value="">Batata</option>
                                                                <option value="">Cebola</option>
                                                                <option value="">Cenoura</option>
                                                                <option value="">Maçã</option>
                                                                <option value="">Melancia</option>
                                                                <option value="">Melão</option>
                                                                <option value="">Morango</option>
                                                                <option value="">Ornamentais</option>
                                                                <option value="">Pepino</option>
                                                                <option value="">Tomate</option>
                                                                <option value="">Uva</option>
                                                            </select>
                                                            <button class="btn btn-danger btn-arrow">▼</button>
                                                </div>                                            
                                            </div>
                                            <div class="col-2">
                                                    <h2 class="section-filter-second-card-body-first-row-title">&nbsp;</h2>
                                                    <a class="search-box-btn" href="">
                                                            <button class="btn btn-dark btn-md btn-search">Buscar</button>
                                                        </a>
                                            </div>
                                            <div class="col-2">
                                                    <h2 class="section-filter-second-card-body-first-row-title">&nbsp;</h2>
                                                    <a class="search-box-btn" href="">
                                                            <button class="btn btn-danger btn-md btn-search">Limpar Filtros</button>
                                                        </a>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                              </div>
                            </div>
                            </div>
                          </div>
        </section>

    
        <section class="market-page-section pt-5">
                <div class="container">
                    
                    <div class="row clearfix">
                            
                        @for($i = 0; $i < 8; $i++)
                        <div class="market-block-four col-md-3 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="lower-content">
                                    <a href="/produtos/produto">
                                        <img style="padding: 0 20px 40px " src="{{asset('images/background/fusao-logo-icon.png')}}" alt="fusão logo">
                                    </a>
                                    <h3><a href="/produtos/produto">Fungicidas  Fusão </a></h3>
                                    <div class="text">Fusão EC é o novo fungicida da IHARA que possui uma molécula...</div>
                                    <a class="link-box" href="/produtos/produto">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
                                </div>
                            </div>
                        </div>
                        @endfor
            
                    </div>
                        
                </div>
                
            </div>
            </section>








          @endsection