    @extends('layout')
    @section('title','Produto')
    @section('content')



    <section class="w-100 section-politica">
    	<h1 class="section-politica-title d-block text-center" style="font-size: 4rem; margin-top: 7rem;">Política de Privacidade</h1>
    	<div class="d-flex container-fluid section-politica-container" style="padding: 5rem 0;">
    		<div id="accordion">
    			<div class="card card-politica">
    				<div class="card-politica-header" id="headingOne">
    					<h5 class="mb-0">
    						<button class="btn card-politica-header-title" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
    							Quem somos
    						</button>
    					</h5>
    					<span class="card-politica-header-icon">+</span>
    				</div>
    				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
    					<div class="card-body" style="font-size:15px;">
    						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
    					</div>
    				</div>
    			</div>
    			<div class="card card-politica">
    				<div class="card-politica-header" id="headingTwo">
    					<h5 class="mb-0">
    						<button class="btn card-politica-header-title collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
    							Porque, quando e como recolhemos seus dados pessoais
    						</button>
    					</h5>
    					<span class="card-politica-header-icon">+</span>
    				</div>
    				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
    					<div class="card-body">
    						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
    					</div>
    				</div>
    			</div>
    			<div class="card card-politica">
    				<div class="card-politica-header" id="headingThree">
    					<h5 class="mb-0">
    						<button class="btn collapsed card-politica-header-title" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
    							Por quanto tempo armazenamos os dados
    						</button>
    					</h5>
    					<span class="card-politica-header-icon">+</span>
    				</div>
    				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
    					<div class="card-body">
    						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
    					</div>
    				</div>
    			</div>
    			<div class="card card-politica">
    				<div class="card-politica-header" id="headingFour">
    					<h5 class="mb-0">
    						<button class="btn collapsed card-politica-header-title" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
    							Os seus direitos como titular de dados
    						</button>
    					</h5>
    					<span class="card-politica-header-icon">+</span>
    				</div>
    				<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
    					<div class="card-body">
    						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
    					</div>
    				</div>
    			</div>
    			<div class="card card-politica">
    				<div class="card-politica-header" id="headingFive">
    					<h5 class="mb-0">
    						<button class="btn collapsed card-politica-header-title" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
    							Cookies de navegador
    						</button>
    					</h5>
    					<span class="card-politica-header-icon">+</span>
    				</div>
    				<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
    					<div class="card-body">
    						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
    					</div>
    				</div>
    			</div>
    			<div class="card card-politica">
    				<div class="card-politica-header" id="headingSix">
    					<h5 class="mb-0">
    						<button class="btn collapsed card-politica-header-title" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
    							Contate-nos
    						</button>
    					</h5>
    					<span class="card-politica-header-icon">+</span>
    				</div>
    				<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
    					<div class="card-body">
    						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    @endsection