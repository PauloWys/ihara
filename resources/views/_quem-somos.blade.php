@extends('layout')
@section('title','Quem Somos')
@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{asset('images/background/6.jpg')}})">
    <div class="auto-container">
        <ul class="page-breadcrumb">
            <li><a href="/home">Home</a></li>
            <li>QUEM SOMOS</li>
        </ul>
        <h1>AGRICULTURA É A NOSSA VIDA</h1>
    </div>
</section>
<!--End Page Title-->

<!--Company Section-->
<section class="company-section">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2>INOVAÇÃO E QUALIDADE JAPONESAS</h2>
                        <div class="separator centered"></div>
                    </div>
                    <div class="bold-text">Contribuindo para a competitividade da agricultura brasileira.</div>
                    <div class="text">
                        <p>Há mais de 50 anos, trabalhamos com os agricultores brasileiros para proteger suas lavouras contra pragas, doenças e plantas daninhas. Fazemos isso sob a cultura japonesa da gestão da qualidade, mantendo um time de vendas altamente técnico e rigorosos padrões de produção. É com base nesses princípios que oferecemos mais de 60 defensivos agrícolas, entre fungicidas, herbicidas, inseticidas e produtos especiais.</p>
                        <p>Entregar soluções é apenas parte do nosso trabalho. Investimos em pesquisa e desenvolvimento para lançar constantemente produtos que atendam às necessidades complexas da agricultura, com produtividade e sustentabilidade.</p>
                    </div>
                    <div class="signature"><img src="{{asset('images/resource/signature.png')}}" alt="" /></div>
                </div>
            </div>

            <!--Image Column-->
            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="image">
                        <img src="{{asset('images/resource/company.jpg')}}" alt="" />
                    </div>
                    <div class="carousel-box">
                        <div class="single-item-carousel owl-carousel owl-box">

                            <div class="testimonial-block-six">
                                <div class="inner">
                                    <div class="quote-icon">
                                        <span class="icon flaticon-left-quote-sketch"></span>
                                    </div>
                                    <div class="text">Com criatividade e espírito pioneiro, faremos do Brasil o maior país agrícola do mundo.
                                    </div>
                                    <div class="author-box">
                                        <div class="author-inner">
                                            <div class="image"><img src="{{asset('images/resource/author-6.jpg')}}" alt="" /></div>
                                            <h3>Nome</h3>
                                            <div class="designation">CEO & Founder</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="testimonial-block-six">
                                <div class="inner">
                                    <div class="quote-icon">
                                        <span class="icon flaticon-left-quote-sketch"></span>
                                    </div>
                                    <div class="text">Com criatividade e espírito pioneiro, faremos do Brasil o maior país agrícola do mundo. </div>
                                    <div class="author-box">
                                        <div class="author-inner">
                                            <div class="image"><img src="{{asset('images/resource/author-6.jpg')}}" alt="" /></div>
                                            <h3>Nome</h3>
                                            <div class="designation">CEO & Founder</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="testimonial-block-six">
                                <div class="inner">
                                    <div class="quote-icon">
                                        <span class="icon flaticon-left-quote-sketch"></span>
                                    </div>
                                    <div class="text">Com criatividade e espírito pioneiro, faremos do Brasil o maior país agrícola do mundo. </div>
                                    <div class="author-box">
                                        <div class="author-inner">
                                            <div class="image"><img src="{{asset('images/resource/author-6.jpg')}}" alt="" /></div>
                                            <h3>Nome</h3>
                                            <div class="designation">CEO & Founder</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--End Company Section-->

<!--Highlights Section-->
<section class="highlights-section">
    <div class="image-layer" style="background-image:url({{asset('images/background/10.jpg')}})"></div>
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <h2>NOSSOS VALORES</h2>
                        <div class="separator centered"></div>
                    </div>

                    <div class="row clearfix">

                        <!--Hightlight Block-->
                        <div class="hightlight-block col-md-4 col-sm-6 col-xs-12">
                            <div class="hightlight-inner">
                                <div class="icon-box">
                                    <span class="icon flaticon-target"></span>
                                </div>
                                <h3>Misão</h3>
                                <a href="/quem-somos" class="overlay-link"></a>
                            </div>
                        </div>

                        <!--Hightlight Block-->
                        <div class="hightlight-block col-md-4 col-sm-6 col-xs-12">
                            <div class="hightlight-inner">
                                <div class="icon-box">
                                    <span class="icon flaticon-graph"></span>
                                </div>
                                <h3>Visão</h3>
                                <a href="/quem-somos" class="overlay-link"></a>
                            </div>
                        </div>

                        <!--Hightlight Block-->
                        <div class="hightlight-block col-md-4 col-sm-6 col-xs-12">
                            <div class="hightlight-inner">
                                <div class="icon-box">
                                    <span class="icon flaticon-quality"></span>
                                </div>
                                <h3>Sustentabilidade</h3>
                                <a href="/quem-somos" class="overlay-link"></a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <!--Services Column-->
            <div class="services-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="upper-box">
                        <div class="icon flaticon-target"></div>
                        <h2>TITULO VALORES</h2>
                    </div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                    <a href="/quem-somos" class="services">VER MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
                </div>
            </div>

        </div>
    </div>
</section>
<!--End Highlights Section-->

<!--History Section-->
<section class="history-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>52 anos de existência</h2>
            <div class="separator"></div>
        </div>

        <div class="history-carousel">
            <div class="carousel-outer">
                <div class="thumbs-outer">
                    <ul class="thumbs-carousel owl-carousel owl-theme">
                        <li>1975</li>
                        <li>1976</li>
                        <li>1977</li>
                        <li>1978</li>
                        <li>1979</li>
                        <li>1980</li>
                        <li>1981</li>
                        <li>1982</li>
                    </ul>
                </div>
                <div class="content-carousel owl-carousel owl-theme">

                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                    <div class="content-slide">
                        <div class="content-inner">
                            <h3>Titulo de Impacto</h3>
                            <div class="date">Fevereiro 11, 1975</div>
                            <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<!--End History Section-->

<!--Call To Action Section-->
<section class="call-to-action-section" style="background-image:url(images/background/5.jpg)">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <h2>Contribuindo para a competitividade da agricultura brasileira.</h2>
            </div>
            <div class="link-column col-md-3 col-sm-12 col-xs-12">
                <a class="apointment" href="/quem-somos">Saiba Mais <span class="icon flaticon-arrow-pointing-to-right"></span></a>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action Section-->

<!--Appointment Form-->
<div class="modal fade" id="schedule-box" tabindex="-1" role="dialog">
  <div class="modal-dialog popup-container container" role="document">
    <div class="modal-content">
        <div class="appoinment_form_wrapper clear_fix">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="get-quote-form" style="background-image:url(images/background/13.jpg)">
                <div class="inner-box">
                    <!--Sec Title-->
                    <div class="sec-title">
                        <div class="clearfix">
                            <div class="pull-left">
                                <h2>Get a Quote</h2>
                                <div class="separator centered"></div>
                            </div>
                            <div class="pull-left">
                                <div class="text">Get a free quote for your industrial or engineering business solutions, We are here 24/7.</div>
                            </div>
                        </div>
                    </div>

                    <!-- Quote Form / Style Two-->
                    <div class="quote-form style-two">
                        <!--Shipping Form-->
                        <form method="post" action="contact.html">
                            <div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="text" placeholder="Your Name" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="text" placeholder="Company Name" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="text" placeholder="Phone" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Select Needed Service</option>
                                        <option>Services One</option>
                                        <option>Services Two</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea placeholder="Your Message..."></textarea>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Now <span class="icon flaticon-arrow-pointing-to-right"></span></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div> 
    </div> 
</div> 
<a href="/home" class="backhome">Voltar pra Home <span class="icon flaticon-arrow-pointing-to-right"></span></a>
</div>
<!-- End of #schedule-box -->

@endsection