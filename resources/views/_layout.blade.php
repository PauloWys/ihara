<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>IHARA - Agricultura é a nossa vida</title>
    <!-- Stylesheets -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/revolution/css/settings.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
    <link href="{{asset('plugins/revolution/css/layers.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
    <link href="{{asset('plugins/revolution/css/navigation.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('css/style2.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">

    <!--<link href="css/color.css" rel="stylesheet">-->

    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <script src="{{asset('js/jquery.js')}}"></script> 
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

    <div class="page-wrapper">

        <!-- Preloader -->
        {{-- <div class="preloader"></div> --}}

        <!-- Main Header-->
        <header class="main-header">

            <!-- Header Top -->
            <div class="header-top">
                <div class="auto-container">
                    <div class="top-outer clearfix">


                        <!--Top Right-->
                        <div class="top-right clearfix">
                            <ul class="clearfix">
                                <li><a href="/trabalhe-conosco">Trabalhe Conosco</a></li>
                                <!--Language-->
                                <li class="language dropdown"><a class="btn btn-default dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="/ptbr"><span class="flag-img"><img src="{{asset('images/icons/flag.jpg')}}" alt="" /></span>Português &nbsp;<span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                                        <li><a href="/en">Inglês</a></li>
                                    </ul>
                                </li>
                            </ul>                        
                        </div>

                    </div>

                </div>
            </div>
            <!-- Header Top End -->

            <!-- Main Box -->
            <div class="main-box">
                <div class="auto-container">
                    <div class="outer-container clearfix">
                        <!--Logo Box-->
                        <div class="logo-box">
                            <div class="logo"><a href="/home"><img src="{{asset('images/logo.png')}}" alt=""></a></div>
                        </div>

                        <!--Nav Outer-->
                        <div class="nav-outer clearfix">

                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->      
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current dropdown"><a href="/home">Home</a>
                                        </li>
                                        <li><a href="/quem-somos">Quem Somos</a></li>

                                        <li class="dropdown has-mega-menu"><a href="/produtos">Produtos</a>
                                            <div class="mega-menu" style="background-image:url(images/background/mega-menu-layer.png)">
                                                <div class="mega-menu-bar row clearfix">
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">
                                                        <h3>Acaricidas</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">MILBEKNOCK</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">OKAY</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">SANMITE EW</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">SANMITE</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">
                                                        <h3>Biológicos</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">ECO-SHOT</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">COSTAR</a></li>
                                                            <!--<li><a href="{{url('/produtos/produto')}}">Produto B3</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">Produto B4</a></li>-->

                                                            </ul>
                                                        </div>
                                                        <div class="column col-md-2 col-sm-3 col-xs-12">
                                                            <h3>Fungicidas</h3>
                                                            <ul>
                                                                <li><a href="{{url('/produtos/produto')}}">APPROVE</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">CERTEZA N</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">COMPLETTO</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">FUSÃO EC</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="column col-md-2 col-sm-3 col-xs-12">
                                                            <h3>Herbicidas</h3>
                                                            <ul>
                                                                <li><a href="{{url('/produtos/produto')}}">TARGA MAX</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">FLUMYZIN 500</a></li> 
                                                                <li><a href="{{url('/produtos/produto')}}">XEQUE MATE</a></li>   
                                                                <li><a href="{{url('/produtos/produto')}}">FACERO SC</a></li>        
                                                            </ul>
                                                        </div>
                                                        <div class="column col-md-2 col-sm-3 col-xs-12">
                                                            <h3>Inseticidas</h3>
                                                            <ul>
                                                                <li><a href="{{url('/produtos/produto')}}">BOLD</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">ELEITTO</a></li>        
                                                            </ul>
                                                        </div>
                                                        <div class="column col-md-2 col-sm-3 col-xs-12">
                                                            <h3>Prod. Especiais</h3>
                                                            <ul>
                                                                <li><a href="{{url('/produtos/produto')}}">IHAROL GOLD</a></li>
                                                                <li><a href="{{url('/produtos/produto')}}">RIPER</a></li>        
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div style="display: flex; align-items: flex-end; justify-content: flex-end;" class="col-md-12"><button class="btn btn-danger btn-lg nav-btn-veja-todos">Veja Todos</button></div>
                                                </div>
                                            </li>
                                            <li class="dropdown has-mega-menu">
                                                <a href="/culturas">Culturas</a>
                                                <div class="mega-menu" style="background-image:url(images/background/mega-menu-layer.png)">
                                                    <div class="mega-menu-bar row">
                                                        <div class="column col-md-2 col-sm-3 col-xs-12">

                                                            <ul>
                                                                <li><a href="{{url('/produtos/produto')}}">ARROZ</a>
                                                                </li>
                                                                    <li><a href="{{url('/produtos/produto')}}">ALGODÃO</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">ARROZ IRRIGADO</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">BATATA</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                <ul>
                                                                    <li><a href="{{url('/produtos/produto')}}">BATATA</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">CAFÉ</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">CANA-DE-AÇUCAR</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">CEBOLA</a></li>

                                                                </ul>
                                                            </div>
                                                            <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                <ul>
                                                                    <li><a href="{{url('/produtos/produto')}}">CITROS</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">FEIJÃO</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">MAÇÃ</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">MILHO</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                <ul>
                                                                    <li><a href="{{url('/produtos/produto')}}">SOJA</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">TOMATE</a></li> 
                                                                    <li><a href="{{url('/produtos/produto')}}">TOMATE(ESTAQUEADO)</a></li>   
                                                                    <li><a href="{{url('/produtos/produto')}}">TOMATE(RASTEIRO)</a></li>        
                                                                </ul>
                                                            </div>
                                                            <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                <ul>
                                                                    <li><a href="{{url('/produtos/produto')}}">TRIGO</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">ABACATE</a></li>        
                                                                </ul>
                                                            </div>
                                                            <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                <ul>
                                                                    <li><a href="{{url('/produtos/produto')}}">ABACAXI</a></li>
                                                                    <li><a href="{{url('/produtos/produto')}}">ABÓBORA</a></li>        
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div style="display: flex; align-items: flex-end; justify-content: flex-end;" class="col-md-12"><button class="btn btn-danger btn-lg nav-btn-veja-todos ">Veja Todos</button>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="dropdown has-mega-menu">
                                                    <a href="/culturas">Alvos</a>
                                                    <div class="mega-menu" style="background-image:url(images/background/mega-menu-layer.png)">
                                                        <div class="mega-menu-bar row">
                                                            <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                <ul>
                                                                    <li><a href="{{url('/produtos/produto')}}">ÁCARO-BRANCO</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">ÁCARO-PURPÚREO</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">BICHO-MINEIRO</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">BOLOR-AZUL</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                    <ul>
                                                                        <li><a href="{{url('/produtos/produto')}}">BROCA DE CANA</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">ANTRACNOSE</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">BICHO-FURÃO</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">AZEVÉM</a></li>

                                                                    </ul>
                                                                </div>
                                                                <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                    <ul>
                                                                        <li><a href="{{url('/produtos/produto')}}">BROCA-DAS-AXILAS</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">CAPIM-ARROZ</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">CIGARRINHA</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">PERCEVEJO</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                    <ul>
                                                                        <li><a href="{{url('/produtos/produto')}}">FERRUGEM</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">ERVA-DE-BICHO</a></li> 
                                                                        <li><a href="{{url('/produtos/produto')}}">GRAMA-SEDA</a></li>   
                                                                        <li><a href="{{url('/produtos/produto')}}">MANCHA-ANGULAR</a></li>        
                                                                    </ul>
                                                                </div>
                                                                <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                    <ul>
                                                                        <li><a href="{{url('/produtos/produto')}}">OIDIO</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">PINTA PRETA</a></li>        
                                                                    </ul>
                                                                </div>
                                                                <div class="column col-md-2 col-sm-3 col-xs-12">

                                                                    <ul>
                                                                        <li><a href="{{url('/produtos/produto')}}">TIRIRICA</a></li>
                                                                        <li><a href="{{url('/produtos/produto')}}">SARNA</a></li>        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div style="display: flex; align-items: flex-end; justify-content: flex-end;" class="col-md-12"><button class="btn btn-danger btn-lg nav-btn-veja-todos ">Veja Todos</button>
                                                            </div>
                                                        </div>


                                                    </li>
                                                    <li><a href="http://iharablog.servidorwys.com.br/" target="_blank">Notícias</a></li>
                                                    <li><a href="/contato" class="theme-btn quote-btn">Contato</a></li>
                                                </ul>
                                            </div>
                                        </nav>
                                        <!-- Main Menu End-->

                                        <!--Outer Box-->
                                        <div class="outer-box">
                                            <ul class="social-icon-one">
                                                <li class="social-icon-separator"> | </li>
                                                <li><a href="/facebook#"><span class="fa fa-facebook"></span></a></li>
                                                <li><a href="/instagram"><span class="fa fa-instagram"></span></a></li>
                                                <li><a href="/twitter"><span class="fa fa-twitter"></span></a></li>
                                                <li><a href="linkedin"><span class="fa fa-linkedin"></span></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <!--Nav Outer End-->

                                </div>    
                            </div>
                        </div>

                        <!--Sticky Header-->
                        <div class="sticky-header">
                            <div class="auto-container">

                                <div class="outer-container clearfix">
                                    <!--Logo Box-->
                                    <div class="logo-box pull-left">
                                        <div class="logo"><a href="/home"><img src="{{asset('images/logo-small.png')}}" alt=""></a></div>
                                    </div>

                                    <!--Nav Outer-->
                                    <div class="nav-outer clearfix">
                                        <!-- Main Menu -->
                                        <nav class="main-menu">
                                            <div class="navbar-header">
                                                <!-- Toggle Button -->      
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                            </div>

                                            <div class="navbar-collapse collapse clearfix">
                                                <ul class="navigation clearfix">
                                                    <li><a href="/home">Home</a></li>
                                                    <li class="dropdown"><a href="/quem-somos">Quem Somos</a> </li>
                                                    <li class="dropdown"><a href="/produtos">Produtos</a> </li>
                                                    <li class="dropdown"><a href="/culturas">Culturas</a> </li>
                                                    <li class="dropdown"><a href="/culturas">Alvos</a> </li>
                                                    <li class="dropdown"><a href="/noticias">Notícias</a> </li>
                                                    <li><a href="/contato">Contato</a></li>
                                                </ul>
                                            </div>
                                        </nav>
                                        <!-- Main Menu End-->

                                    </div>
                                    <!--Nav Outer End-->

                                </div>

                            </div>
                        </div>
                        <!--End Sticky Header-->

                    </header>
                    <!--End Main Header -->

                    @yield('content')

                    <!--Main Footer-->
                    <footer class="main-footer">
                        <div class="auto-container">
                            <!--Widgets Section-->
                            <div class="widgets-section">
                                <div class="row clearfix">

                                    <!--Column-->
                                    <div class="column col-md-5 col-sm-6 col-xs-12">
                                        <div class="footer-widget logo-widget">
                                            <div class="logo">
                                                <a href="/home"><img src="{{asset('images/footer-logo.png')}}" alt="" /></a>
                                            </div>
                                            <div class="widget-content">
                                                <div class="text">Av. Liberdade, 1701 Cajuru do Sul Sorocaba - SP CEP: 18087-170</div>
                                                <ul class="list">
                                                    <li>+55 (15) 3235-7700 </li>
                                                    <li>contato@ihara.com.br</li>
                                                </ul>
                                                <div class="timing">
                                                    <span>Ihara Sorocaba</span>Seg - Sex: 8:00 - 17:00
                                                </div>
                                                <ul class="social-icon-one">
                                                    <li><a href="/facebook"><span class="fa fa-facebook"></span></a></li>
                                                    <li><a href="/twitter"><span class="fa fa-twitter"></span></a></li>
                                                    <li><a href="/instagram"><span class="fa fa-instagram"></span></a></li>
                                                    <li><a href="/linkedin"><span class="fa fa-linkedin"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Column-->
                                    <div class="column col-md-4 col-sm-6 col-xs-12">
                                        <div class="footer-widget links-widget">
                                            <div class="footer-title">
                                                <h2>Acesso Rápido</h2>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="column col-md-6 col-sm-6 col-xs-12">
                                                    <ul class="links">
                                                        <li><a href="/quem-somos">Quem Somos</a></li>
                                                        <li><a href="/culturas">Culturas</a></li>
                                                        <li><a href="/sustentabilidade">Sustentabilidade</a></li>
                                                        <li><a href="/noticias">Notícias</a></li>
                                                        <li><a href="/contato">Contato</a></li>
                                                    </ul>
                                                </div>
                                                <div class="column col-md-6 col-sm-6 col-xs-12">
                                                    <ul class="links">
                                                        <li><a href="/imprensa">Imprensa</a></li>
                                                        <li><a href="/trabalhe-conosco">Trabalhe Conosco</a></li>
                                                        <li><a href="/area-cliente">Área Exclusiva Cliente</a></li>
                                                        <li><a href="/area-colaborador">Área Exclusiva Colaborador</a></li>
                                                        <li><a href="/politica-privacidade">Política de Privacidade</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="footer-bottom">
                            <div class="auto-container">
                                {{-- <div class="row clearfix">

                                    <!--Title Column-->
                                    <div class="title-column col-md-5 col-sm-12 col-xs-12">
                                        <div class="text"><span class="icon flaticon-rss-updates-subscription"></span> Acompanhe as principais notícias sobre agricultura em nosso site.</div>
                                    </div>
                                    <!--Subscribe Column-->
                                    <div class="subscribe-column col-md-7 col-sm-12 col-xs-12">
                                        <div class="subscribe-form">
                                            <form method="post" action="contact.html">
                                                <div class="form-group">
                                                    <input type="email" name="email" value="" placeholder="Coloque seu e-mail" required="">
                                                    <button type="Enviar" class="text-white theme-btn">Inscreva-se <span class="flaticon-right-arrow-1"></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div> --}}

                                <div class="copyright">Desenvolvido por Agência Wys</div>

                            </div>
                        </div>
                    </footer>

                </div>
                <!--End pagewrapper-->

                <!--Scroll to top-->
                <div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>




                <!--Revolution Slider-->
                <script src="{{asset('plugins/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
                <script src="{{asset('plugins/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
                <script src="{{asset('js/main-slider-script.js')}}"></script>

                <script src="{{asset('js/validate.js')}}"></script>
                <script src="{{asset('js/bootstrap.min.js')}}"></script>
                <script src="{{asset('js/jquery.fancybox.js')}}"></script>
                <script src="{{asset('js/owl.js')}}"></script>
                <script src="{{asset('js/jquery-ui.js')}}"></script>
                <script src="{{asset('js/wow.js')}}"></script>
                <script src="{{asset('js/knob.js')}}"></script>
                <script src="{{asset('js/appear.js')}}"></script>
                <script src="{{asset('js/script.js')}}"></script>



                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                {{-- <link href="{{asset('fontawesome-free-5.9.0-web/css/all.css')}}" rel="stylesheet"> <!--load all styles --> --}}
                {{-- <script src="{{asset('fontawesome-free-5.9.0-web/js/all.js')}}"></script> --}}

                <!--Google Map APi Key-->
                <script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
                <script src="{{asset('js/map-script.js')}}"></script>
                <!--End Google Map APi-->

                <script> $(function () {$('[data-toggle="popover"]').popover() }) </script>

            </body>
            </html>