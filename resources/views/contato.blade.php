@extends('layout')
@section('title','Contato')
@section('content')
<!--Location Section-->
<section class="location-section" style="background-image:url(images/background/pattern-1.png)">
    <div class="auto-container">
        <ul class="page-breadcrumb">
            <li><a href="/home"></a></li>
            <li class="text-white"></li>
        </ul>
        <div class="sec-title centered light">
            <h2>ENTRE EM CONTATO COM A IHARA</h2>
            
        </div>


    </div>

</div>
</section>
<!--End Page Title-->

<section class="contact-section">
    <div class="auto-container">

        <!--Title Box-->
        <div class="sec-title">
            <div class="clearfix">
                <div class="pull-left">
                    <h2>CONTATOS</h2>
                    <div class="separator"></div>
                </div>
                <div class="text" style="top:45px; right:125px">  Use o canal abaixo para se manifestar.</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 div-envelope">
             <i class="fa fa-envelope fa-5x" aria-hidden="true"></i>
             <h1>E-mail</h1>
             <p>Tem alguma dúvida? Podemos te ajudar pelo nosso canal de email.</p>
             <a href="mailto:contato@ihara.com.br">contato@ihara.com.br</a>


         </div>

         <div class="col-md-4 col-sm-12 div-envelope">
             <i class="fa fa-phone fa-5x"></i>
             <h1>Telefone</h1>
             <p>Tem alguma dúvida? Podemos te ajudar pelo nosso canal de email.</p>
             <a href="tel:1532357777">(15) 3235-7777</a>

         </div>
         <div class="col-md-4 col-sm-12 div-envelope">

             <i class="fa fa-map-marker fa-5x" aria-hidden="true"></i>
             <h1>Localização</h1>
             <p>Tem alguma dúvida? Podemos te ajudar pelo nosso canal de email. </p>
             <a target="_blank" href="https://www.google.com.br/maps/place/IHARA/@-23.4257505,-47.3734734,17z/data=!3m1!4b1!4m5!3m4!1s0x94cf5e2d7489e141:0x1925c12a3fb17f76!8m2!3d-23.4257554!4d-47.3712847">Mapa</a>
         </div>


     </div>
     <div style="margin-bottom: 39px;"  class="separa"></div>
     <div class="row">
        <div  class="col-md-12 col-sm-12 ihara-sociais"><h1>Siga a Ihara nas redes sociais</h1>
          <p style="max-width: 26%;">Tem alguma dúvida? Podemos te ajudar pelo nosso canal de email.</p>
          <div class="rede-ssocial">
            <a target="_blank" href="https://pt-br.facebook.com/iharadefensivosagricolas/"><i class="fa fa-facebook fa-3x"></i></a>
            <a target="_blank" href=""><i class="fa fa-twitter fa-3x"></i></a>
            <a target="_blank" href="https://www.instagram.com/iharadefensivosagricolas/"><i class="fa fa-instagram fa-3x"></i></a>
            <a target="_blank" href="https://www.linkedin.com/company/iharabras-s-a-industrias-quimicas/?originalSubdomain=br"><i class="fa fa-linkedin fa-3x"></i></a>

        </div>
    </div>


</div>
<div style="margin-bottom: 39px;"  class="separa"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 trabalhe-conosco">
        <h1>Trabalhe Conosco</h1>
        <p style="max-width: 26%;">Consulte nossas oportunidades clicando no icone abaixo.</p>
        <a target="_blank" href="https://site.vagas.com.br/PagEmpr.asp?e=ihara"><i class="fa fa-briefcase fa-3x"></i></a>
        
        
    </div>
</div>
<div style="margin-bottom: 39px;"  class="separa"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ouvidoria">
        <h1>Ouvidoria</h1>
        <p style="max-width: 40%;">A ouvidoria é um canal sigiloso e seguro para que qualquer colaborador, fornecedor, cliente ou parceiro possa denunciar eventual falta de ética, indisciplina ou ato de improbidade relacionada à empresa.</p>

        <a href="mailto:contato@ihara.com.br"><i class="fa fa-comment fa-3x"></i></a>
        
    </div>

</div>
</div>
</section>
{{-- 
    <!--Quote Form-->
    <div class="modal fade" id="schedule-box" tabindex="-1" role="dialog">
      <div class="modal-dialog popup-container container" role="document">
        <div class="modal-content">
            <div class="appoinment_form_wrapper clear_fix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="get-quote-form" style="background-image:url(images/background/13.jpg)">
                    <div class="inner-box">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2>Get a Quote</h2>
                                    <div class="separator centered"></div>
                                </div>
                                <div class="pull-left">
                                    <div class="text">Get a free quote for your industrial or engineering business solutions, We are here 24/7.</div>
                                </div>
                            </div>
                        </div>

                        <!-- Quote Form / Style Two-->
                        <div class="quote-form style-two">
                            <!--Shipping Form-->
                            <form method="post" action="contact.html">
                                <div class="row clearfix">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="text" placeholder="Seu Nome" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="text" placeholder="Nome da empresa" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="text" placeholder="Telefone" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <select class="custom-select-box">
                                            <option>Escolha o serviço que precisa</option>
                                            <option>SAC</option>
                                            <option>Ouvidoria</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <textarea placeholder="Mensagem..."></textarea>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Now <span class="icon flaticon-arrow-pointing-to-right"></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div> 
        </div> 
    </div> 
    <a href="/home" class="backhome">Back to Home <span class="icon flaticon-arrow-pointing-to-right"></span></a>
</div>
<!-- End of Quote box --> --}}

@endsection