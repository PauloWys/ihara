           @extends('layout')
           @section('title','Produto')
           @section('content')


           <!--Page Title-->
           <section class="page-title" style="background-image:url('/cmsihara/public/banners/{{$produto->banner}}')">
            <div class="auto-container">
                <div class="row">
                    <div class="col-md-3 offset-4 text-center">

                    </div>
                    <div class="col-md-5">
                    </div>
                </div>
            </div>
        </section>
        <!--End Page Title-->

        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <!--Market Single-->
                        <div class="market-single">
                            <div class="inner-box">
                                <div class="image">

                                </div>
                                <div class="lower-content">
                                    <h2 id="title-eco-shot">{{$produto->slogan}}</h2>
                                    <ul class="page-breadcrumb">
                                        <li>Biológicos</li>
                                        <li class="breadcrumb-eco-shot">{{$produto->nome}}</li>
                                    </ul>
                                    <div class="text">
                                        <p>{{$produto->text_descri}}</p>
                                    </div>
                                    {{-- BENEFICIOS --}}
                                    <h3 class="title">Benefícios</h3>

                                    <div id="" class="row text-center mb-5 pt-5 pb-5 eco-shot-icons"
                                    style="font-size: 1.3em">


                                    <div class="col-md-2 col-12 mb-4 eco-shot-icons-icon">

                                        <img class="mb-3 bleed" src='/cmsihara/public/icon/benef/{{$produto->img_benef1}}'>
                                        <p class="mb-0 normal-line">{{$produto->texto_benef1}}</p><br>

                                    </div>
                                    <div class="col-md-2 col-12 mb-4 eco-shot-icons-icon">

                                        <img class="mb-3 bleed" src='/cmsihara/public/icon/benef/{{$produto->img_benef2}}'>
                                        <p class="mb-0 normal-line">{{$produto->texto_benef2}}</p>

                                    </div>
                                    <div class="col-md-2 col-12 mb-4 eco-shot-icons-icon">

                                        <img class="mb-3 bleed" src='/cmsihara/public/icon/benef/{{$produto->img_benef3}}'>
                                        <p class="mb-0 normal-line">{{$produto->texto_benef3}}</p>

                                    </div>
                                    <div class="col-md-2 col-12 mb-4 eco-shot-icons-icon">

                                        <img class="mb-3 bleed" src='/cmsihara/public/icon/benef/{{$produto->img_benef4}}'>
                                        <p class="mb-0 normal-line">{{$produto->texto_benef4}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar market-sidebar">

                        <div class="sidebar-title">
                            <h2>FICHA TÉCNICA</h2>
                        </div>
                        <!--Blog Category Widget-->
                        <table class="table table-ficha-tecnica">
                            <tbody>
                                <tr>
                                    <td class="table-cel-title">Ingrediente Ativo</td>
                                    <td>Bacillus amyloliquefaciens cepa D-747</td>
                                </tr>
                                <tr>
                                    <td class="table-cel-title">Classe Ambiental</td>
                                    <td>IV</td>
                                </tr>
                                <tr>
                                    <td class="table-cel-title">Concentração</td>
                                    <td>250 g/kg</td>
                                </tr>
                                <tr>
                                    <td class="table-cel-title">Tipo de Formulação</td>
                                    <td>Granulado Dispersível (WG)</td>
                                </tr>
                                <tr>
                                    <td class="table-cel-title">Classe Toxicológica</td>
                                    <td>III</td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- Sidebar brochure-->
                        <div class="sidebar-widget sidebar-brochure">
                            <div class="sidebar-title">
                                <h2>MATERIAIS</h2>
                            </div>
                            <a class="brochure"
                            href="http://www.ihara.com.br/upload/produtos/fispq/1516030813.pdf"><span
                            class="icon flaticon-pdf"></span>FISP
                            <span>Clique aqui para baixar</span></a>
                            <a class="brochure"
                            href="http://www.ihara.com.br/upload/produtos/fet/1539692368.pdf"><span
                            class="icon flaticon-pdf"></span>FET<span>Clique aqui para baixar</span></a>
                            <a class="brochure"
                            href="http://www.ihara.com.br/upload/produtos/bula/1557516605.pdf"><span
                            class="icon flaticon-pdf"></span>Bula
                            <span>Clique aqui para baixar</span></a>
                            <a class="brochure"
                            href="http://www.ihara.com.br/upload/produtos/outros/1507728173.pdf"><span
                            class="icon flaticon-pdf"></span>Folder Eco-Shot
                            <span>Clique aqui para baixar</span></a>
                            <a class="brochure"
                            href="http://www.ihara.com.br/upload/produtos/outros/1519407053.pdf"><span
                            class="icon flaticon-pdf"></span>Boletim Técnico
                            <span>Clique aqui para baixar</span></a>
                        </div>
                    </aside>
                </div>
                {{-- END SIDEBAR SIDE --}}
            </div>
        </div>

        {{-- CONTENT AFTER SIDEBAR STYLE --}}
        <section>
            <div class="container video-destaque col-12">
                <h3 class="title video-title text-center">Saiba mais sobre o {{$produto->nome }}</h3>

                <div class="iframe-banner">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_destaq}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div>    
                <div class="col-12 d-flex justify-content-center video-destaque-container-call-to-action">
                   <button class="btn btn-danger btn-call-to-action">Quero saber mais <img src="{{asset('images/download-icon.png')}}" alt=""></button>
               </div>
           </div> 
       </section>








       {{-- <div class="resultados">
        <h3 class="resultados-title text-center">Confira os <br><span class="resultados-destaque">resultados</span></h3>
        <div class="w-100 container-resultados-img"><img style="width: 100%;" src="images/grafico.png" alt="" style ="margin-bottom: ;"></div>                
    </div> --}}







    <div class="container noticias container-videos col-12">
        <h3 class="title text-center">Veja vídeos relacionados ao {{$produto->nome }}</h3>
        <div class="container-noticias row section-videos-row">
            <div class="col-lg-4">
                <div class="section-videos-iframe">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_rel1}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div> 
                <!--<p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white">Biológio Ecoshot - Mais tempo de prateleira</p>-->
            </div>
            <div class="col-lg-4">
                <div class="section-videos-iframe">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_rel2}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div> 
                <!--<p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white">Biológio Ecoshot - Mais tempo de prateleira</p>-->
            </div>
            <div class="col-lg-4">
                <div class="section-videos-iframe">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_rel3}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div> 
                <!--<p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white">Biológio Ecoshot - Mais tempo de prateleira</p>-->
            </div>
        </div>
        <div class="container-noticias row section-videos-row">
            <div class="col-lg-4">
                <div class="section-videos-iframe">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_usa1}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div> 
                <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white">Quem usa Aprova</p>
            </div>
            <div class="col-lg-4">
                <div class="section-videos-iframe">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_usa2}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div> 
                <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white">Quem usa Aprova</p>
            </div>
            <div class="col-lg-4">
                <div class="section-videos-iframe">
                    <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_usa3}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
                </div> 
                <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white">Quem usa Aprova</p>

            </div>
        </div>
    </div>






    <section class="section-indicacoes">
        <div class="container col-12">
            <h3 class="title text-center section-indicacoes-title">Veja as culturas para os quais o Eco-Shot é indicado</h3>
            <div class="container col-10 section-indicacoes-container-select">
                <div class="indicacoes-box">
                    <select name="" id="">
                        <option value="">Abóbora</option>
                        <option value="">Abobrinha</option>
                        <option value="">Alface</option>
                        <option value="">Alho</option>
                        <option value="">Batata</option>
                        <option value="">Cebola</option>
                        <option value="">Cenoura</option>
                        <option value="">Maçã</option>
                        <option value="">Melancia</option>
                        <option value="">Melão</option>
                        <option value="">Morango</option>
                        <option value="">Ornamentais</option>
                        <option value="">Pepino</option>
                        <option value="">Tomate</option>
                        <option value="">Uva</option>
                    </select>
                    <button class="btn btn-danger btn-arrow">▼</button>
                    <button class="btn btn-danger btn-call-to-action btn-call-to-action-indicacoes">Quero saber mais <img src="{{asset('images/download-icon.png')}}" alt=""></button>
                </div>
            </div>
        </div>


    </section>





    <div class="noticias">
        <h3 class="title noticias-title text-center">Veja também</h3>
        <div class="container-noticias">
            <div class="col-lg-3">
                <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                    class="noticias-link"
                    href="http://www.ihara.com.br/noticias/noticias-de-mercado/mt-aumenta-a-preocupacao-com-armazenamento/4278">MT:
                aumenta a preocupação com armazenamento</a> </h4>
                <p>A produção de milho no Estado vem ganhando força e já alcançou 31,08 milhões de
                toneladas este ano... </p>
            </div>
            <div class="col-lg-3">
                <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                    class="noticias-link"
                    href="http://www.ihara.com.br/noticias/ihara-na-midia/proximidade-do-periodo-umido-aumenta-preocupacao-com-o-greening-no/4277">Proximidade
                do período úmido aumenta preocupação com o greening no campo</a> </h4>
                <p>Afetando mais de 37 milhões de laranjeiras do cinturão citrícola de São Paulo e
                Triângulo/Sudoeste Mineiro...</p>

            </div>
            <div class="col-lg-3">
                <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                    class="noticias-link"
                    href="http://www.ihara.com.br/noticias/noticias-de-mercado/aumento-do-dolar-fez-esmagadoras-comprarem-mais-soja/4276">
                Aumento do dólar fez esmagadoras comprarem mais soja</a></h4>
                <p>As indústrias esmagadoras compraram a maior parte das 700 mil toneladas
                negociadas ontem, devido ao aumento...</p>
            </div>
        </div>
    </div>

</div>


{{-- BARRA SUBSCRIPTION --}}

{{-- <section class="call-to-action-section" style="background-image:url(images/background/5.jpg)">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Title Column-->
            <div class="title-column col-md-5 col-sm-12 col-xs-12">
                <div class="text"><span class="icon flaticon-rss-updates-subscription"></span> Cadastre-se aqui para receber notícias do agronegócio</div>
            </div>
            <!--Subscribe Column-->
            <div class="subscribe-column col-md-7 col-sm-12 col-xs-12">
                <div class="subscribe-form">
                    <form method="post" action="contact.html">
                        <div class="form-group">
                            <input class="subscribe-email" type="email" name="email" value="" placeholder="Coloque seu e-mail" required="">
                            <button type="Enviar" class="text-white theme-btn" data-modal="abrir">Inscreva-se <span class="flaticon-right-arrow-1"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section> --}}


<section class="section-subscribe">
    <div class="container" style="display: flex">
        <div class="col-8">
            <h3 class="title section-subscribe-title">Quer receber mais informações sobre o Eco-Shot ?</h3>
        </div>
        <div class="col-4" style="display: flex; justify-content: center; align-items:center;">
            <button class="theme-btn btn-style-one btn-subscribe" type="" name="" data-modal="abrir">Inscreva-se<span class="icon flaticon-arrow-pointing-to-right"></span></button>
        </div>
    </div>
</section>

</div>

{{-- MODAL --}}
<section class="modal-container-form" data-modal="container">
    <div class="modal-form">
        <button data-modal="fechar" class="fechar">X</button>
        <form action="">
            <label for="name">Seu nome completo</label>
            <input type="text" for="name" name="" id="name">
            <label for="phone">Seu melhor telefone</label>
            <input type="text" for="phone" name="" id="phone">
            <label for="email">Seu melhor e-mail</label>
            <input type="text" for="email" name="" id="email">
            <label for="city">Cidade</label>
            <input type="text" for="city" name="" id="city">
            <label for="state">Estado</label>
            <input type="text" for="state" name="" id="state">
            <div style="display:flex; justify-content: center; align-items:center;">
                <input class="input-checkbox" type="checkbox" name="" id="politic" for="politic" style="">
                <label for="politic">Li e concordo com a <a href="">política de privacidade</a></label>
            </div>
            <button class="btn btn-block" type="submit">Quero saber mais</button>
        </form>
    </div>
</section>

{{-- JAVASCRIPT MODAL --}}

<script>

    const botaoAbrir = document.querySelector('[data-modal="abrir"]');
    const botaoFechar = document.querySelector('[data-modal="fechar"]');
    const containerModal = document.querySelector('[data-modal="container"]');

    if(botaoAbrir && botaoFechar && containerModal) {
        function abrirModal (event) {
            event.preventDefault();
            containerModal.classList.add('ativo');
        }

        function fecharModal () {
            event.preventDefault();
            containerModal.classList.remove('ativo');
        }

        function cliqueForaModal (event) {
            if(event.target === this)
                fecharModal(event);
        }

        botaoAbrir.addEventListener('click', abrirModal);
        botaoFechar.addEventListener('click', fecharModal);
        containerModal.addEventListener('click', cliqueForaModal);
    }



</script>


@endsection