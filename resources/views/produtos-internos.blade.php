    @extends('layout')
    @section('title','Produto')
    @section('content')


    <!--Page Title--> 
    @if($produto->banner != '')
    <section class="page-title" style="background-image:url('/cmsihara/public/banners/{{$produto->banner}}')">
        <div class="auto-container">
            <div class="row">
                <div class="col-md-3 offset-4 text-center">

                </div>
                <div class="col-md-5">
                </div>
            </div>
        </div>
    </section>
    @else    
    <section class="page-title page-title-prod-min" style="height:80vh !important; background-image:url(http://localhost/ihara/public/images/background/template-site_2.jpg">
     <div class="row clearfix" style="width: 85%;">
        <div class="col-md-12" style="display: flex;align-items: center;justify-content: flex-end;">
            <img style="height: 28%; width:33%;" src="/cmsihara/public/banners/{{$produto->logo}}" alt="" srcset="">


        </div>
    </div>
</section>
@endif
<!--End Page Title-->

<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <!--Market Single-->
                <div class="market-single">
                    <div class="inner-box">
                        <div class="image">

                        </div>
                        <div class="lower-content">
                            <h2 id="title-eco-shot" style="color:{{$cor}};">{{$produto->slogan}}</h2>
                            <ul class="page-breadcrumb">
                                <li style="color:{{$cor}}; margin-right: 0; padding-right: 0;">{{$nome}}-></li>
                                <li class="breadcrumb-eco-shot" style="color:{{$cor}};">{{$produto->nome}}</li>
                            </ul>
                            <div class="text">
                                <!--<p>{{$produto->text_descri}}</p>-->
                                <p>{{ $produto->text_descri }}</p>


                            </div>
                            {{-- BENEFICIOS --}}
                            @if( $produto->text_descri != '' && $produto->img_benef1 != '' && $produto->texto_benef1 != '')
                            <h3 class="title">Benefícios</h3>

                            <div id="" class="row text-center mb-5 pt-5 pb-5 eco-shot-icons"
                            style="font-size: 1.3em">

                            @if($produto->img_benef1 != '')
                            <div class="col-md-3 col-12 mb-4 eco-shot-icons-icon">

                                <img class="mb-3 bleed" style="max-width: 75px;" src='/cmsihara/public/icon/benef/{{$produto->img_benef1}}'>
                                <p class="mb-0 normal-line" style="height: 35px;">{{$produto->texto_benef1}}</p><br>

                            </div>
                            @endif
                            @if($produto->img_benef2 != '')
                            <div class="col-md-3 col-12 mb-4 eco-shot-icons-icon">

                                <img class="mb-3 bleed" style="max-width: 75px;" src='/cmsihara/public/icon/benef/{{$produto->img_benef2}}'>
                                <p class="mb-0 normal-line">{{$produto->texto_benef2}}</p>

                            </div>
                            @endif
                            @if($produto->img_benef3 != '')
                            <div class="col-md-3 col-12 mb-4 eco-shot-icons-icon">

                                <img class="mb-3 bleed" style="max-width: 75px;" src='/cmsihara/public/icon/benef/{{$produto->img_benef3}}'>
                                <p class="mb-0 normal-line">{{$produto->texto_benef3}}</p>

                            </div>
                            @endif
                            @if($produto->img_benef4 != '')
                            <div class="col-md-3 col-12 mb-4 eco-shot-icons-icon" >

                                <img class="mb-3 bleed" style="max-width: 75px;" src='/cmsihara/public/icon/benef/{{$produto->img_benef4}}'>
                                <p class="mb-0 normal-line">{{$produto->texto_benef4}}</p>
                            </div>
                            @endif
                        </div>
                        @else
                        <section class="section-indicacoes" style="background-color: {{$cor}};">
                            <div class="container col-12">
                                <h3 class="title text-center section-indicacoes-title">Veja as culturas para os quais o {{$produto->nome}} é indicado</h3>
                                <div class="container col-10 section-indicacoes-container-select">
                                    <div class="indicacoes-box" style="width: 290px !important">
                                        <select class="" name="" id="" style="background: white; color:black">
                                            <option value="">Algodão</option>
                                        </select>
                                        {{-- <button class="btn btn-danger btn-arrow">▼</button> --}}
                                    </div>
                                </div>
                            </div>


                        </section>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <!--Sidebar Side-->
        <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <aside class="sidebar market-sidebar">

                <div class="sidebar-title">
                    <h2>FICHA TÉCNICA</h2>
                </div>
                <!--Blog Category Widget-->
                <table class="table table-ficha-tecnica">
                    <tbody>
                        <tr>
                            <td class="table-cel-title">Ingrediente Ativo</td>
                            <td>{{$produto->ing_ativo}}</td>
                        </tr>
                        <tr>
                            <td class="table-cel-title">Classe Ambiental</td>
                            <td>{{$produto->class_amb}}</td>
                        </tr>
                        <tr>
                            <td class="table-cel-title">Concentração</td>
                            <td>{{$produto->concentracao}}</td>
                        </tr>
                        <tr>
                            <td class="table-cel-title">Tipo de Formulação</td>
                            <td>{{$produto->tipo_form}}</td>
                        </tr>
                        <tr>
                            <td class="table-cel-title">Classe Toxicológica</td>
                            <td>{{$produto->class_tox}}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- Sidebar brochure-->
                @if( $materiais != NULL )
                <div class="sidebar-widget sidebar-brochure">
                    <div class="sidebar-title">
                        <h2>MATERIAIS</h2>
                    </div>
                    @foreach($materiais as $material)
                    <a class="brochure"
                    href="/cmsihara/public/folders/{{$material->arquivo}}"><span
                    class="icon flaticon-pdf"></span>{{$material->nome}}
                    <span>Clique aqui para baixar</span></a>
                    @endforeach

                </div>
                @endif
            </aside>
        </div>
        {{-- END SIDEBAR SIDE --}}
    </div>
</div>

{{-- CONTENT AFTER SIDEBAR STYLE --}}
<section>
    @if($produto->video_destaq != '')
    <div class="container video-destaque col-12" style="background-color:{{$cor}};">
        <h3 class="title video-title text-center">Saiba mais sobre o {{$produto->nome }}</h3>

        <div class="iframe-banner">
            <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_destaq}}?rel=0" frameborder="0" allow="" allowfullscreen></iframe>
        </div>    
        <div class="col-12 d-flex justify-content-center video-destaque-container-call-to-action">
           <button style="border: white;background: white;color: black;" class="btn btn-danger btn-call-to-action" data-modal="abrir">Baixar o material Técnico <i class="fa fa-download"></i></button>
       </div>
   </div> 
   @endif
</section>








{{-- <div class="resultados">
    <h3 class="resultados-title text-center">Confira os <br><span class="resultados-destaque">resultados</span></h3>
    <div class="w-100 container-resultados-img"><img style="width: 100%;" src="images/grafico.png" alt="" style ="margin-bottom: ;"></div>                
</div> --}}



@if($produto->video_rel1 != '' && $produto->video_rel2 != '' && $produto->video_rel3 != '' && $produto->video_rel3 != '' && $produto->video_usa1 != '' && $produto->video_usa2 != '' && $produto->video_usa3 != '' )

<div class="container noticias container-videos col-12">
    <h3 class="title text-center">Veja vídeos relacionados ao {{$produto->nome }}</h3>
    <div class="container-noticias row section-videos-row">

        @if($produto->video_rel1 != '')
        <div class="col-lg-4">
            <div class="section-videos-iframe">
                <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_rel1}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
            </div> 
            <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white"></p>
        </div>
        @endif
        @if($produto->video_rel2 != '')
        <div class="col-lg-4">
            <div class="section-videos-iframe">
                <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_rel2}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
            </div> 
            <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white"></p>
        </div>
        @endif
        @if($produto->video_rel3 != '')
        <div class="col-lg-4">
            <div class="section-videos-iframe">
                <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_rel3}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
            </div> 
            <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white"></p>
        </div>
        @endif  
    </div>
    <div class="container-noticias row section-videos-row">
        @if($produto->video_usa1 != '')
        <div class="col-lg-4">
            <div class="section-videos-iframe">
                <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_usa1}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
            </div> 
            <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white"></p>
        </div>
        @endif
        @if($produto->video_usa2 != '')
        <div class="col-lg-4">
            <div class="section-videos-iframe">
                <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_usa2}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
            </div> 
            <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white"></p>
        </div>
        @endif
        @if($produto->video_usa3 != '')
        <div class="col-lg-4">
            <div class="section-videos-iframe">
                <iframe class="" width="500" height="412" src="https://www.youtube.com/embed/{{$produto->video_usa3}}?rel=0" frameborder="0" allow=""" allowfullscreen></iframe>
            </div> 
            <p class="text-center section-videos-subtitle" style="color: white; border-top: 1px solid white"></p>

        </div>
        @endif
    </div>
</div>
@else
<div class="clearfix"></div>

@endif

<style>
    .icon-cult{
        width: 30px;
        height: 30px;
        position:relative;
        float: left;
        bottom: 3px;
    }
    .img-text p{
        position:static;
    }
</style>
{{--END-STYLE--}}

@if( $produto->text_descri != '' && $produto->img_benef1 != '' && $produto->texto_benef1 != '')

<section class="section-indicacoes" style="background-color:{{$cor}};">
 <div class="container col-12">
    <h3 class="title text-center section-indicacoes-title">Veja as culturas para os quais o {{$produto->nome}} é indicado</h3>
    <div class="container col-10 section-indicacoes-container-select">
        <div class="indicacoes-box">
            <select name="" id="">
                <option value="">Abóbora</option>
                <option value="">Abobrinha</option>
                <option value="">Alface</option>
                <option value="">Alho</option>
                <option value="">Batata</option>
                <option value="">Cebola</option>
                <option value="">Cenoura</option>
                <option value="">Maçã</option>
                <option value="">Melancia</option>
                <option value="">Melão</option>
                <option value="">Morango</option>
                <option value="">Ornamentais</option>
                <option value="">Pepino</option>
                <option value="">Tomate</option>
                <option value="">Uva</option>
            </select>
            <button class="btn btn-danger btn-arrow">▼</button>
      
        </div>
    </div>
</div>


</section>
@endif

<section class="section-subscribe">
    <div class="container" style="display: flex">
        <div class="col-8">
            <h3 class="title section-subscribe-title">Quer receber mais informações sobre o {{$produto->nome}}?</h3>
        </div>
        <div class="col-4" style="display: flex; justify-content: center; align-items:center;">
            <button class="theme-btn btn-style-one btn-subscribe" type="" name="" data-modal="abrir">Inscreva-se<span class="icon flaticon-arrow-pointing-to-right"></span></button>
        </div>
    </div>
</section>




<div class="noticias">
    <h3 class="title noticias-title text-center">Veja também</h3>
    <div class="container-noticias">
        <div class="col-lg-3">
            <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                class="noticias-link"
                href="http://www.ihara.com.br/noticias/noticias-de-mercado/mt-aumenta-a-preocupacao-com-armazenamento/4278">MT:
            aumenta a preocupação com armazenamento</a> </h4>
            <p>A produção de milho no Estado vem ganhando força e já alcançou 31,08 milhões de
            toneladas este ano... </p>
        </div>
        <div class="col-lg-3">
            <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                class="noticias-link"
                href="http://www.ihara.com.br/noticias/ihara-na-midia/proximidade-do-periodo-umido-aumenta-preocupacao-com-o-greening-no/4277">Proximidade
            do período úmido aumenta preocupação com o greening no campo</a> </h4>
            <p>Afetando mais de 37 milhões de laranjeiras do cinturão citrícola de São Paulo e
            Triângulo/Sudoeste Mineiro...</p>

        </div>
        <div class="col-lg-3">
            <h4 style="color: black;"> <a style="color: black; font-weight: 600;"
                class="noticias-link"
                href="http://www.ihara.com.br/noticias/noticias-de-mercado/aumento-do-dolar-fez-esmagadoras-comprarem-mais-soja/4276">
            Aumento do dólar fez esmagadoras comprarem mais soja</a></h4>
            <p>As indústrias esmagadoras compraram a maior parte das 700 mil toneladas
            negociadas ontem, devido ao aumento...</p>
        </div>
    </div>
</div>

</div>


{{-- BARRA SUBSCRIPTION --}}

{{-- <section class="call-to-action-section" style="background-image:url(images/background/5.jpg)">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Title Column-->
            <div class="title-column col-md-5 col-sm-12 col-xs-12">
                <div class="text"><span class="icon flaticon-rss-updates-subscription"></span> Cadastre-se aqui para receber notícias do agronegócio</div>
            </div>
            <!--Subscribe Column-->
            <div class="subscribe-column col-md-7 col-sm-12 col-xs-12">
                <div class="subscribe-form">
                    <form method="post" action="contact.html">
                        <div class="form-group">
                            <input class="subscribe-email" type="email" name="email" value="" placeholder="Coloque seu e-mail" required="">
                            <button type="Enviar" class="text-white theme-btn" data-modal="abrir">Inscreva-se <span class="flaticon-right-arrow-1"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section> --}}




</div>

{{-- MODAL --}}
<section class="modal-container-form" data-modal="container">
    <div class="modal-form">
        <button data-modal="fechar" class="fechar">X</button>
        <form action="" name="frmEnvia" onSubmit="return(verifica())">

            {{ csrf_field() }}
            <label for="name">Seu nome completo</label>
            <input type="text" for="name" name="" id="name">
            <label for="phone">Seu melhor telefone</label>
            <input type="text" class="telefone" name="telefone" id="telefone">
            <label for="email">Seu melhor e-mail</label>
            <input type="text" name="email" id="email" onblur="checarEmail()">
            <label for="">Cidade</label>
            <select class="form-control" id="cod_cidades">
                <option value="">-- Escolha um estado --</option>
            </select>

            <label for="state">Estado</label>
            <select class="form-control" id="cod_estados">
                @foreach($estados as $estado)
                <option class="" value="{{$estado->cod_estados}}">{{$estado->sigla}}</option>
                @endforeach
            </select>
            <!--<input type="text"  name="" id="state">-->
            <div style="display:flex;  align-items:center;">
                <input class="input-checkbox" type="checkbox" name="" id="politic" for="politic" style="">
                <label for="politic">Li e concordo com a <a href="">política de privacidade</a></label>
            </div>
            <div style="display:flex; align-items:center;">
                <input class="input-checkbox" type="checkbox" name="" id="infos" for="infos" style="">
                <label for="infos">Aceito receber informações da IHARA</label>
            </div>
            <button class="btn btn-block" type="submit">Quero saber mais</button>
        </form>
    </div>
</section>

{{-- JAVASCRIPT MODAL --}}
<script src="{{asset('js/jquery.mask.min.js')}}"></script>    
<script>
    $(function(){
        $('.img-wrapper').hover(function(){
            $(this).find('.img-text').slideDown(200);
        },function(){
            $(this).find('.img-text').slideUp(200);
        });
    })

                // const botaoAbrir = document.querySelector('[data-modal="abrir"]');
                const botaoAbrir = document.querySelectorAll('[data-modal="abrir"]');
                const botaoFechar = document.querySelector('[data-modal="fechar"]');
                const containerModal = document.querySelector('[data-modal="container"]');

                if(botaoAbrir && botaoFechar && containerModal) {
                    function abrirModal (event) {
                        event.preventDefault();
                        containerModal.classList.add('ativo');
                    }

                    function fecharModal () {
                        event.preventDefault();
                        containerModal.classList.remove('ativo');
                    }

                    function cliqueForaModal (event) {
                        if(event.target === this)
                            fecharModal(event);
                    }

                    // botaoAbrir.addEventListener('click', abrirModal);
                    for(let i=0; i < botaoAbrir.length; i++){
                        botaoAbrir[i].addEventListener('click', abrirModal);
                    }
                    botaoFechar.addEventListener('click', fecharModal);
                    containerModal.addEventListener('click', cliqueForaModal);
                }

                function verifica() {
                  if (document.forms[0].email.value.length == 0) {
                    alert('Por favor, informe o seu EMAIL.');
                    document.frmEnvia.email.focus();
                    return false;
                }
                return true;
            }

            function checarEmail(){
                if( document.forms[0].email.value=="" 
                   || document.forms[0].email.value.indexOf('@')==-1 
                   || document.forms[0].email.value.indexOf('.')==-1 )
                {
                  alert( "Por favor, informe um E-MAIL válido!" );
                  return false;
                  
              }
          }

          document.getElementById('telefone').addEventListener('blur', function (e) {
              var x = e.target.value.replace(/\D/g, '').match(/(\d{2})(\d{5})(\d{4})/);
              e.target.value = '(' + x[1] + ') ' + x[3] + '-' + x[3];
          });

          $('#telefone').mask('(00) 0000-00009');

          $('#cod_estados').change(function() {

            if( $(this).val() ) {

                //alert($(this).val());

                $.ajax({
                    url: '/ihara/public/cidades-estado/'+ $(this).val(),
                    type: 'GET',
                    dataType: 'json'
                    //data: {param1: 'value1'},
                })
                .done(function(data) {

                 var options = '<option value=""></option>';
                 for(var i=0; i < data.cidades.length; i++){
                    options += '<option value="'+data.cidades[i]['cod_cidades'] +'">'+data.cidades[i]['nome']+'<option>';
                }

                //alert(options);

                $('#cod_cidades').html(options).show();
                $('.carregando').hide();


            })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            }

        });

         /* $(function(){
              $('#cod_estados').change(function(){
                if( $(this).val() ) {   
                  $('#cod_cidades').hide();
                  $('.carregando').show();
                  $.getJSON(
                    '/ihara/public/cidades-estado/'+$(this).val(),
                    {
                      cod_estados: $(this).val(),
                      ajax: 'true'
                  }, function(j){
                      var options = '<option value=""></option>';
                      for (var i = 0; i < j.length; i++) {
                        options += '<option value="' +
                        j[i].cod_cidades + '">' +
                        j[i].nome + '</option>';
                    }
                    $('#cod_cidades').html(options).show();
                    $('.carregando').hide();
                });
              } else {
                  $('#cod_cidades').html(
                    '<option value="">-- Escolha um estado --</option>'
                    );
              }
          });
      });*/

  </script>


  @endsection