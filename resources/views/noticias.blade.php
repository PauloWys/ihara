@extends('layout')
@section('title','Noticias')
@section('content')
<!--Page Title-->
<section class="page-title" style="background-image:url({{asset('images/background/6.jpg')}})">
  <div class="auto-container">
    <ul class="page-breadcrumb">
      <li><a href="/home">Home</a></li>
      <li>NOTÍCIAS</li>
    </ul>
    <h1>Últimas Notícias</h1>
  </div>
</section>
<!--End Page Title-->

<!--Blog Page Section-->
<section class="blog-page-section">
  <div class="auto-container">

    <div class="row clearfix">

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

      <!--News Block Two-->
      <div class="news-block-two col-md-4 col-sm-6 col-xs-12">
        <div class="inner-box">
          <div class="image">
            <a href="/noticias"><img src="images/resource/news-4.jpg" alt="" /></a>
          </div>
          <div class="lower-content">
            <h3><a href="/noticias">Plantio do trigo avança no Paraná</a></h3>
            <div class="title">ihara news</div>
            <div class="post-date">Daniela Marketing, 11 de Agosto 2019</div>
            <div class="text">Rationally encounter consequences seds ut that are extremely painful...</div>
            <a href="/noticias" class="read-more">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
          </div>
        </div>
      </div>

    </div>

    <!--Styled Pagination-->
    <ul class="styled-pagination text-center">
      <li class="prev"><a href="#"><span class="fa fa-angle-left"></span></a></li>
      <li><a href="#" class="active">1</a></li>
      <li><a href="#">2</a></li>
      <li class="next"><a href="#"><span class="fa fa-angle-right"></span></a></li>
    </ul>                
    <!--End Styled Pagination-->

  </div>
</section>
<!--End Blog Section Four-->


<!--End History Section-->

<!--Call To Action Section-->
<section class="call-to-action-section" style="background-image:url(images/background/5.jpg)">
  <div class="auto-container">
    <div class="row clearfix">
      <div class="col-md-9 col-sm-12 col-xs-12">
        <h2>Contribuindo para a competitividade da agricultura brasileira.</h2>
      </div>
      <div class="link-column col-md-3 col-sm-12 col-xs-12">
        <a class="apointment" href="/quem-somos">Saiba Mais <span class="icon flaticon-arrow-pointing-to-right"></span></a>
      </div>
    </div>
  </div>
</section>
<!--End Call To Action Section-->

<!--Appointment Form-->
<div class="modal fade" id="schedule-box" tabindex="-1" role="dialog">
  <div class="modal-dialog popup-container container" role="document">
    <div class="modal-content">
      <div class="appoinment_form_wrapper clear_fix">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
        <div class="get-quote-form" style="background-image:url(images/background/13.jpg)">
          <div class="inner-box">
            <!--Sec Title-->
            <div class="sec-title">
              <div class="clearfix">
                <div class="pull-left">
                  <h2>Get a Quote</h2>
                  <div class="separator centered"></div>
                </div>
                <div class="pull-left">
                  <div class="text">Get a free quote for your industrial or engineering business solutions, We are here 24/7.</div>
                </div>
              </div>
            </div>

            <!-- Quote Form / Style Two-->
            <div class="quote-form style-two">
              <!--Shipping Form-->
              <form method="post" action="contact.html">
                <div class="row clearfix">
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="text" placeholder="Your Name" required>
                  </div>
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="text" placeholder="Company Name" required>
                  </div>
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="text" placeholder="Phone" required>
                  </div>
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <select class="custom-select-box">
                      <option>Select Needed Service</option>
                      <option>Services One</option>
                      <option>Services Two</option>
                    </select>
                  </div>
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <textarea placeholder="Your Message..."></textarea>
                  </div>
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Now <span class="icon flaticon-arrow-pointing-to-right"></span></button>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div> 
    </div> 
  </div> 
  <a href="/home" class="backhome">Voltar pra Home <span class="icon flaticon-arrow-pointing-to-right"></span></a>
</div>
<!-- End of #schedule-box -->
@endsection