<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>IHARA - Agricultura é a nossa vida</title>
    <!-- Stylesheets -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/revolution/css/settings.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
    <link href="{{asset('plugins/revolution/css/layers.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
    <link href="{{asset('plugins/revolution/css/navigation.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('css/style2.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('js/OwlCarousel2-2.3.4/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('js/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css')}}" rel="stylesheet">

    <!--<link href="css/color.css" rel="stylesheet">-->

    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{asset('js/jquery.js')}}"></script> 
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

    <div class="page-wrapper">

        <!-- Preloader -->
        {{-- <div class="preloader"></div> --}}

        <!-- Main Header-->
        <header class="main-header">

            <!-- Header Top -->
            <div class="header-top">
                <div class="auto-container">
                    <div class="top-outer clearfix">


                        <!--Top Right-->
                        <div class="top-right clearfix">
                            <ul class="clearfix">
                                <li><a target="_blank" href="https://site.vagas.com.br/PagEmpr.asp?e=ihara">Trabalhe Conosco</a></li>
                                <!--Language-->
                                <li class="language dropdown"><a class="btn btn-default dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="/ptbr"><span class="flag-img"><img src="{{asset('images/icons/flag.jpg')}}" alt="" /></span>Português &nbsp;<span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                                        <li><a href="/en">Inglês</a></li>
                                    </ul>
                                </li>
                            </ul>                        
                        </div>

                    </div>

                </div>
            </div>
            <!-- Header Top End -->

            <!-- Main Box -->
            <div class="main-box">
                <div class="auto-container">
                    <div class="outer-container clearfix">
                        <!--Logo Box-->
                        <div class="logo-box">
                            <div class="logo"><a href="/home"><img src="{{asset('images/logo.png')}}" alt=""></a></div>
                        </div>

                        <!--Nav Outer-->
                        <div class="nav-outer clearfix">

                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->      
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current dropdown"><a href="/home">Home</a>
                                        </li>
                                        <li><a href="/quem-somos">Quem Somos</a></li>

                                        <li class="dropdown has-mega-menu"><a href="/produtos">Produtos</a>
                                            <div class="mega-menu" style="background-image:url(images/background/mega-menu-layer.png)">
                                                <div class="mega-menu-bar row clearfix">
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">
                                                        <h3>FUNGICIDAS</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Approve</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Fusão EC</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Certeza N</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Totalit</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">
                                                        <h3>INSETICIDAS</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Zeus</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Maxsan</a></li> 
                                                            <li><a href="{{url('/produtos/produto')}}">Spirit</a></li>    
                                                            <li><a href="{{url('/produtos/produto')}}">Bold</a></li>       
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">
                                                        <h3>HERBICIDAS</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Flumyzin 500</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Targa Max</a></li> 
                                                            <li><a href="{{url('/produtos/produto')}}">Magneto</a></li>   
                                                            {{-- Será ativado somente após registro --}}
                                                            {{-- <li><a href="{{url('/produtos/produto')}}">Yamato</a></li>         --}}
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">
                                                        <h3>BIOLÓGICOS</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Eco-Shot</a></li>
                                                        </ul>
                                                    </div>                              
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">
                                                        <h3 style="font-size: 19px;">PROD. ESPECIAIS</h3>
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Iharol Gold</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Frutoil</a></li>     
                                                            <li><a href="{{url('/produtos/produto')}}">Riper</a></li> 
                                                            <li><a href="{{url('/produtos/produto')}}">Viviful SC</a></li>      
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div style="display: flex; align-items: flex-end; justify-content: flex-end;" class="col-md-11"><button class="btn btn-danger btn-lg nav-btn-veja-todos">Veja Todos</button></div>
                                            </div>
                                        </li>
                                        <li class="dropdown has-mega-menu">
                                            <a href="/culturas">Culturas</a>
                                            <div class="mega-menu" style="background-image:url(images/background/mega-menu-layer.png)">
                                                <div class="mega-menu-bar row">
                                                    <div class="mega-menu-bar-colum column col-md-3 col-sm-3 col-xs-12">
                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Algodão</a>
                                                            </li>
                                                            <li><a href="{{url('/produtos/produto')}}">Arroz</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Batata</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Café</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Cana</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Citros</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Feijão</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Maçã</a></li>

                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Milho</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Soja</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Tomate</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Trigo</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">xxx</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">xxx</a></li> 
                                                            <li><a href="{{url('/produtos/produto')}}">xxx</a></li>   
                                                            <li><a href="{{url('/produtos/produto')}}">xxx</a></li>        
                                                        </ul>
                                                    </div>
                                                    {{-- <div class="column col-md-2 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">TRIGO</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">ABACATE</a></li>        
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-2 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">ABACAXI</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">ABÓBORA</a></li>        
                                                        </ul>
                                                    </div> --}}
                                                </div>
                                                <div style="display: flex; align-items: flex-end; justify-content: flex-end;" class="col-md-12"><button class="btn btn-danger btn-lg nav-btn-veja-todos ">Veja Todos</button>
                                                </div>
                                            </div>
                                        </li>


                                        {{-- "Alternaria", "Antracnose", "Bicho Mineiro", "Brusone", "Cigarrinha", "Ferrugem Asiática", "Lagarta", "Mofo Branco", "Mosca Branca", "Mosca-da-frutas", "Nematoides", "Percevejo", "Psilídeo", "Requeima" --}}
                                        <li class="dropdown has-mega-menu">
                                            <a href="/culturas">Alvos</a>
                                            <div class="mega-menu" style="background-image:url(images/background/mega-menu-layer.png)">
                                                <div class="mega-menu-bar row">
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Alternaria</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Antracnose</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Bicho Mineiro</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Brusone</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Cigarrinha</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Ferrugem asiática</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Lagarta</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Mofo branco</a></li>

                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Mosca branca</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Mosca-da-fruta</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Nematoide</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Percevejo</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column col-md-3 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">Psilídeo</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">Requeima</a></li> 
                                                            <li><a href="{{url('/produtos/produto')}}">xxx</a></li>   
                                                            <li><a href="{{url('/produtos/produto')}}">xxx</a></li>        
                                                        </ul>
                                                    </div>
                                                    {{-- <div class="column col-md-2 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">OIDIO</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">PINTA PRETA</a></li>        
                                                        </ul>
                                                    </div> --}}
                                                    {{-- <div class="column col-md-2 col-sm-3 col-xs-12">

                                                        <ul>
                                                            <li><a href="{{url('/produtos/produto')}}">TIRIRICA</a></li>
                                                            <li><a href="{{url('/produtos/produto')}}">SARNA</a></li>        
                                                        </ul>
                                                    </div> --}}
                                                </div>
                                                <div style="display: flex; align-items: flex-end; justify-content: flex-end;" class="col-md-12"><button class="btn btn-danger btn-lg nav-btn-veja-todos ">Veja Todos</button>
                                                </div>
                                            </div>


                                        </li>
                                        <li><a href="http://iharablog.servidorwys.com.br/" target="_blank">Notícias</a></li>
                                        <li><a href="/contato" class="theme-btn quote-btn">Contato</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->

                            <!--Outer Box-->
                            <div class="outer-box">
                                <ul class="social-icon-one">
                                    <li class="social-icon-separator"> | </li>
                                    <li><a href="/facebook#"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="/instagram"><span class="fa fa-instagram"></span></a></li>
                                    <li><a href="/twitter"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="linkedin"><span class="fa fa-linkedin"></span></a></li>
                                </ul>
                            </div>

                        </div>
                        <!--Nav Outer End-->

                    </div>    
                </div>
            </div>

            <!--Sticky Header-->
            <div class="sticky-header">
                <div class="auto-container">

                    <div class="outer-container clearfix">
                        <!--Logo Box-->
                        <div class="logo-box pull-left">
                            <div class="logo"><a href="/home"><img src="{{asset('images/logo-small.png')}}" alt=""></a></div>
                        </div>

                        <!--Nav Outer-->
                        <div class="nav-outer clearfix">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->      
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li><a href="/home">Home</a></li>
                                        <li class="dropdown"><a href="/quem-somos">Quem Somos</a> </li>
                                        <li class="dropdown"><a href="/produtos">Produtos</a> </li>
                                        <li class="dropdown"><a href="/culturas">Culturas</a> </li>
                                        <li class="dropdown"><a href="/culturas">Alvos</a> </li>
                                        <li class="dropdown"><a href="/noticias">Notícias</a> </li>
                                        <li><a href="/contato">Contato</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->

                        </div>
                        <!--Nav Outer End-->

                    </div>

                </div>
            </div>
            <!--End Sticky Header-->

        </header>
        <!--End Main Header -->

        @yield('content')

        <!--Main Footer-->
        <footer class="main-footer">
            <div class="auto-container">
                <!--Widgets Section-->
                <div class="widgets-section">
                    <div class="row clearfix">

                        <!--Column-->
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo">
                                    <a href="/home"><img src="{{asset('images/footer-logo.png')}}" alt="" /></a>
                                </div>
                                <div class="widget-content">
                                    <div class="text" style="font-size: 15px;">Av. Liberdade, 1701 Cajuru do Sul Sorocaba - SP CEP: 18087-170</div>
                                    <ul class="list">
                                        <li>+55 (15) 3235-7700 </li>
                                        <li>contato@ihara.com.br</li>
                                    </ul>
                                    <div class="timing">
                                        <!--<span>Ihara</span>-->
                                    </div>
                                    <ul class="social-icon-one">
                                        <li><a href="/facebook"><span class="fa fa-facebook fa-2x"></span></a></li>
                                        <li><a href="/twitter"><span class="fa fa-twitter fa-2x"></span></a></li>
                                        <li><a href="/instagram"><span class="fa fa-instagram fa-2x"></span></a></li>
                                        <li><a href="/linkedin"><span class="fa fa-linkedin fa-2x"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!--Column-->
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <div class="footer-title">
                                    <h2>Acesso Rápido</h2>
                                </div>
                                <div class="row clearfix">
                                    <div class="column col-md-6 col-sm-6 col-xs-12">
                                        <ul class="links">
                                            <li><a href="/quem-somos">Quem Somos</a></li>
                                            <li><a href="/culturas">Culturas</a></li>
                                            <li><a href="/sustentabilidade">Sustentabilidade</a></li>
                                            <li><a href="/noticias">Notícias</a></li>
                                            <li><a href="/contato">Contato</a></li>
                                        </ul>
                                    </div>
                                    <div class="column col-md-6 col-sm-6 col-xs-12">
                                       <ul class="links">
                                           <li><a href="/imprensa">Imprensa</a></li>
                                           <li><a target="_blank" href="https://site.vagas.com.br/pagEmpr.asp?e=ihara">Trabalhe Conosco</a></li>
                                           <li><a href="http://cliente.ihara.com.br">Área Exclusiva Cliente</a></li>
                                           <li><a href="http://portal.ihara.com.br/">Área Exclusiva Colaborador</a></li>
                                           <li><a href="http://localhost/ihara/public//politica-privacidade">Política de Privacidade</a></li>
                                       </ul>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="column col-md-6 col-sm-6 col-xs-12">
                        <img src="/ihara/public/images/andef_branco.png">
                    </div>

                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                {{-- <div class="row clearfix">

                    <!--Title Column-->
                    <div class="title-column col-md-5 col-sm-12 col-xs-12">
                        <div class="text"><span class="icon flaticon-rss-updates-subscription"></span> Acompanhe as principais notícias sobre agricultura em nosso site.</div>
                    </div>
                    <!--Subscribe Column-->
                    <div class="subscribe-column col-md-7 col-sm-12 col-xs-12">
                        <div class="subscribe-form">
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="email" name="email" value="" placeholder="Coloque seu e-mail" required="">
                                    <button type="Enviar" class="text-white theme-btn">Inscreva-se <span class="flaticon-right-arrow-1"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> --}}

                <div class="copyright">Desenvolvido por Agência Wys</div>

            </div>
        </div>
    </footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>




<!--Revolution Slider-->
<script src="{{asset('plugins/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('plugins/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{asset('js/main-slider-script.js')}}"></script>

<script src="{{asset('js/validate.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/owl.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/wow.js')}}"></script>
<script src="{{asset('js/knob.js')}}"></script>
<script src="{{asset('js/appear.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script src="{{asset('js/OwlCarousel2-2.3.4/dist/owl.carousel.js')}}"></script>
<script src="{{asset('js/OwlCarousel2-2.3.4/dist/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/OwlCarousel2-2.3.4/src/js/owl.navigation.js')}}"></script>
<script src="{{asset('js/OwlCarousel2-2.3.4/src/js/owl.animate.js')}}"></script>



<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
{{-- <link href="{{asset('fontawesome-free-5.9.0-web/css/all.css')}}" rel="stylesheet"> <!--load all styles --> --}}
{{-- <script src="{{asset('fontawesome-free-5.9.0-web/js/all.js')}}"></script> --}}

<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="{{asset('js/map-script.js')}}"></script>
<!--End Google Map APi-->

<script> $(function () {$('[data-toggle="popover"]').popover() }) </script>

<script>
    $('.tparrows.gyges').css('top','30%');
</script>
</body>
</html>