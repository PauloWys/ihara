@extends('layout')
@section('title','Contato')
@section('content')



<section class="div-susten" style="background-image:url({{asset('images/images-sustentabilidade.jpg')}})">
	


</section>
<div class="h2-banner text-center"><h2>A Ihara acredita no desenvolvimento sustentável</h2></div>
<section class="contact-section" style="margin: 20px;">
	<div class="auto-container">

		<!--Title Box-->
		<div class="sec-title">
			<div class="clearfix">
				<div class="row">
					<div class="pull-left col-md-6 col-sm-12 text-ini">

						<h2 style="font-size: 18px;">O mundo precisará cada vez mais de alimentos. Sempre respeitaremos os recursos naturais na busca pela produtividade. Tudo está interligado: Terra, Água e Ar são os maiores bens da humanidade.</h2>
						<p style="color: #6b6868;">Teremos a máxima responsabilidade na forma de tratá-los. Alimentar o mundo preservando esses bens é a verdadeira sustentabilidade.</p>
						<div class="separator"></div>
					</div>
					<div class="col-md-6 col-sm-12" style="margin-top: -107px;"><img src="/ihara/public/images/eco-direita.png" alt=""></div>
				</div>
				<div style="margin: 50px 0px;"  class="separa"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 text-susten text-center" >
						<h2>Sustentabilidade</h2>
						<p style="margin-top: 40px;">A IHARA acredita que é possível conciliar crescimento econômico com desenvolvimento sustentável. Por isso, ao longo de sua história, sempre foi marcante sua atuação nos aspectos econômico, social e ambiental, pilares da sustentabilidade. A IHARA possui um departamento específico que reúne todas as iniciativas relacionadas aos assuntos e projetos em Sustentabilidade. Acesse abaixo nossos Relatórios de Sustentabilidade.</p>


						
					</div>
				</div>

				<style>
					.container-download {
						position: relative;
						width: 50%;
					}

					.image-download {
						opacity: 1;
						display: block;
						width: 100%;
						height: auto;
						transition: .5s ease;
						backface-visibility: hidden;
					}

					.middle-download {
						transition: .5s ease;
						opacity: 0;
						position: absolute;
						top: 50%;
						left: 50%;
						transform: translate(-50%, -50%);
						-ms-transform: translate(-50%, -50%);
						text-align: center;
					}

					.container-download:hover .image-download {
						opacity: 0.3;
					}

					.container-download:hover .middle-download {
						opacity: 1;
					}

					.text-download {
						background-color:rgba(165, -11, 13, 0.91);
						color: white;
						font-size: 16px;
						padding: 16px 32px;
					}
				</style>
				<div class="row">
					<div class="col-md-4 col-sm-12" style="margin-top: 50px;">
						
						<div class="container-download">
							<a target="_blank" href="http://www.ihara.com.br/upload/relatorios/arquivos/1541759416.pdf">
								<div class="download-titulo"><h2 style="padding: 0; font-size: 14px;">Relatorio de sustentabilidade 2009</h2></div>
								<img src="/ihara/public/images/relatorio.jpg" alt="Avatar" class="image-download" style="width:100%">

								<div class="middle-download">
									<div class="text-download"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
								</div>
							</a>
						</div>

					</div>
					<div class="col-md-4 col-sm-12" style="margin-top: 50px;">
						
						<div class="container-download">
							<a target="_blank" href="http://www.ihara.com.br/upload/relatorios/arquivos/1542027838.pdf">
								<div class="download-titulo"><h2 style="padding: 0; font-size: 14px;">Relatorio de sustentabilidade 2009</h2></div>
								<img src="/ihara/public/images/relatorio2.jpg" alt="Avatar" class="image-download" style="width:100%">

								<div class="middle-download">
									<div class="text-download"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
								</div>
							</a>
						</div>

					</div>
					<div class="col-md-4 col-sm-12" style="margin-top: 50px;">
						
						<div class="container-download">
							<a target="_blank" href="http://www.ihara.com.br/upload/relatorios/arquivos/1541759766.pdf">
								<div class="download-titulo"><h2 style="padding: 0; font-size: 14px;">Relatorio de sustentabilidade 2009</h2></div>
								<img src="/ihara/public/images/relatorio3.jpg" alt="Avatar" class="image-download" style="width:100%">

								<div class="middle-download">
									<div class="text-download"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
								</div>
							</a>
						</div>

					</div>
					<div class="col-md-4 col-sm-12" style="margin-top: 50px;">
						
						<div class="container-download">
							<a target="_blank" href="http://www.ihara.com.br/upload/relatorios/arquivos/1541759809.pdf">
								<div class="download-titulo"><h2 style="padding: 0; font-size: 14px;">Relatorio de sustentabilidade 2009</h2></div>
								<img src="/ihara/public/images/relatorio4.jpg" alt="Avatar" class="image-download" style="width:100%">

								<div class="middle-download">
									<div class="text-download"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
								</div>
							</a>
						</div>

					</div>
					<div class="col-md-4 col-sm-12" style="margin-top: 50px;">
						
						<div class="container-download">
							<a target="_blank" href="http://www.ihara.com.br/upload/relatorios/arquivos/1541760006.pdf">
								<div class="download-titulo"><h2 style="padding: 0; font-size: 14px;">Relatorio de sustentabilidade 2009</h2></div>
								<img src="/ihara/public/images/relatorio5.jpg" alt="Avatar" class="image-download" style="width:100%">

								<div class="middle-download">
									<div class="text-download"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
								</div>
							</a>
						</div>

					</div>
					<div class="col-md-4 col-sm-12" style="margin-top: 50px;"s>
						
						<div class="container-download">
							<a target="_blank" href="http://www.ihara.com.br/upload/relatorios/arquivos/1541759937.pdf">
								<div class="download-titulo"><h2 style="padding: 0; font-size: 14px;">Relatorio de sustentabilidade 2009</h2></div>
								<img src="/ihara/public/images/relatorio5.jpg" alt="Avatar" class="image-download" style="width:100%">

								<div class="middle-download">
									<div class="text-download"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
								</div>
							</a>
						</div>

					</div>
				</div>

				
			</div>
		</div>

	</div>
</section>
<!--<section class="page-title div-susten" style="background-image:url({{asset('images/images-sustentabilidade.jpg')}})">
	<div class="auto-container">
		<ul class="page-breadcrumb">

		</ul>
		<h1 style="">AGRICULTURA É A NOSSA VIDA</h1>
	</div>
</section>-->
@endsection