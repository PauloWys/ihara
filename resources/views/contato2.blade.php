@extends('layout')
@section('title','Contato')
@section('content')
    <!--Location Section-->
    <section class="location-section" style="background-image:url(images/background/pattern-1.png)">
        <div class="auto-container">
            <ul class="page-breadcrumb">
                <li><a href="/home">Home</a></li>
                <li class="text-white">CONTATO</li>
            </ul>
            <div class="sec-title centered light">
                <h2>SERVIÇOS DE ATENDIMENTO AO CLIENTE</h2>
                <div class="separator"></div>
            </div>
            
            
        </div>
        
    </div>
</section>
<!--End Page Title-->

<section class="contact-section">
    <div class="auto-container">
        
        <!--Title Box-->
        <div class="sec-title">
            <div class="clearfix">
                <div class="pull-left">
                    <h2>FALE CONOSCO</h2>
                    <div class="separator"></div>
                </div>
                <div class="text" style="top:45px; right:170px">  Use o canal abaixo para se manifestar.</div>
            </div>
        </div>
        
        <div class="inner-container">
            <div class="clearfix">
                
                <!--Info Column-->
                <div class="info-column col-md-4 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="upper-box" style="left:780px;height:292px">
                            <ul class="list-style-three">
                                <li><span class="icon flaticon-technology-2"></span><strong>Telefone: </strong>+55 (15) 3235-7700</li>
                                <li><span class="icon flaticon-envelope-1"></span><strong>E-mail : </strong>ouvidoria@ihara.com.br</li>
                                <li><span class="icon flaticon-time-1"></span><strong>Horário: </strong>Segunda - Sabado: 8:00 as 17:00</li>
                            </ul>
                        </div>
                    </div>
                </div>
    
                <!--Form Column-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12" style="right: 386px">
                    <div class="inner-column">
                        
                        <!-- Contact Form -->
                        <div class="contact-form">
                            <!--Default Form-->
                            <form method="post" action="sendemail.php" id="contact-form">
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="username" placeholder="Nome" required>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="email" name="email" placeholder="E-Mail" required>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="phone" placeholder="Telefone" required>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <select>
                                        <option value="Assunto">Selecione o assunto</option>
                                        <option value="SAC">SAC</option>
                                        <option value="Fornecedores">Fornecedores</option>
                                        <option value="Ouvidoria">Ouvidoria</option>
                                        </select>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="message" placeholder="Mensagem"></textarea>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn submit-btn" type="submit" name="submit-form">ENVIAR MENSAGEM <span class="icon flaticon-right-arrow-1"></span></button>
                                    </div>
                                    
                                </div>
                            </form>
                            
                        </div>
                        <!--End Contact Form -->
                        
                    </div>
                </div>
                
            </div>
        </div>
</section>
{{-- 
<!--Quote Form-->
<div class="modal fade" id="schedule-box" tabindex="-1" role="dialog">
  <div class="modal-dialog popup-container container" role="document">
    <div class="modal-content">
        <div class="appoinment_form_wrapper clear_fix">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="get-quote-form" style="background-image:url(images/background/13.jpg)">
                <div class="inner-box">
                    <!--Sec Title-->
                    <div class="sec-title">
                        <div class="clearfix">
                            <div class="pull-left">
                                <h2>Get a Quote</h2>
                                <div class="separator centered"></div>
                            </div>
                            <div class="pull-left">
                                <div class="text">Get a free quote for your industrial or engineering business solutions, We are here 24/7.</div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Quote Form / Style Two-->
                    <div class="quote-form style-two">
                        <!--Shipping Form-->
                        <form method="post" action="contact.html">
                            <div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="text" placeholder="Seu Nome" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="text" placeholder="Nome da empresa" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="text" placeholder="Telefone" required>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <select class="custom-select-box">
                                        <option>Escolha o serviço que precisa</option>
                                        <option>SAC</option>
                                        <option>Ouvidoria</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea placeholder="Mensagem..."></textarea>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Now <span class="icon flaticon-arrow-pointing-to-right"></span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div> 
    </div> 
</div> 
<a href="/home" class="backhome">Back to Home <span class="icon flaticon-arrow-pointing-to-right"></span></a>
</div>
    <!-- End of Quote box --> --}}

@endsection