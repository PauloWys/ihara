@extends('layout')
@section('title','Home')
@section('content')

<!--Main Slider-->

<style type="text/css">

    .hove-text{

    }
    .hove-text:hover{

      color: white !important;


  }
</style>
<section class="main-slider">

    <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
        <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
            <ul>

                <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-1.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="{{asset('images/main-slider/image-1.jpg')}}"> 

                    <div class="tp-caption" 
                    data-paddingbottom="[2,2,1,1]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[200,200,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['650','700','650','460']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-80','-90','-70','-80']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                    style="z-index: 7; white-space: nowrap;">
                    <h2 style="padding: 200px 0 !important;">Maxsan</h2>
                </div>

                <div class="tp-caption" 
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-type="text"
                data-height="none"
                data-width="['650','700','650','460']"
                data-whitespace="normal"
                data-hoffset="['15','15','15','15']"
                data-voffset="['80','60','60','40']"
                data-x="['left','left','left','left']"
                data-y="['middle','middle','middle','middle']"
                data-textalign="['top','top','top','top']"
                data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                style="z-index: 7; white-space: nowrap;">
                <div style="font-size: 22px !important;" class="text">poder de outro mundo no combate à cigarrinha.</div>
            </div>

            <div class="tp-caption" 
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingtop="[0,0,0,0]"
            data-responsive_offset="on"
            data-type="text"
            data-height="none"
            data-width="['550','550','550','460']"
            data-whitespace="normal"
            data-hoffset="['15','15','15','15']"
            data-voffset="['150','130','130','130']"
            data-x="['left','left','left','left']"
            data-y="['middle','middle','middle','middle']"
            data-textalign="['top','top','top','top']"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
            style="z-index: 7; white-space: nowrap;">
            <a href="/quem-somos" class="know_more"><span class="icon flaticon-right-arrow-1"></span> saiba mais</a>
        </div>

    </li>

    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="{{asset('images/main-slider/image-2.jpg')}}"> 

        
        <div class="tp-caption" 
        data-paddingbottom="[2,2,1,1]"
        data-paddingleft="[0,0,0,0]"
        data-paddingright="[0,0,0,0]"
        data-paddingtop="[200,200,0,0]"
        data-responsive_offset="on"
        data-type="text"
        data-height="none"
        data-width="['650','700','650','460']"
        data-whitespace="normal"
        data-hoffset="['15','15','15','15']"
        data-voffset="['-80','-90','-70','-80']"
        data-x="['left','left','left','left']"
        data-y="['middle','middle','middle','middle']"
        data-textalign="['top','top','top','top']"
        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
        style="z-index: 7; white-space: nowrap;">
        <h2 style="padding: 200px 0 !important;">Zeus</h2>
    </div>

    <div class="tp-caption" 
    data-paddingbottom="[0,0,0,0]"
    data-paddingleft="[0,0,0,0]"
    data-paddingright="[0,0,0,0]"
    data-paddingtop="[0,0,0,0]"
    data-responsive_offset="on"
    data-type="text"
    data-height="none"
    data-width="['650','700','650','460']"
    data-whitespace="normal"
    data-hoffset="['15','15','15','15']"
    data-voffset="['80','60','60','40']"
    data-x="['left','left','left','left']"
    data-y="['middle','middle','middle','middle']"
    data-textalign="['top','top','top','top']"
    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
    style="z-index: 7; white-space: nowrap;">
    <div style="font-size: 22px !important;" class="text">proteção nunca antes vista que combate os percevejos e eleva a sua produtividade.
    </div>
</div>

<div class="tp-caption" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['550','550','550','460']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['150','130','130','130']"
data-x="['left','left','left','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
style="z-index: 7; white-space: nowrap;">
<a href="/quem-somos" class="know_more"><span class="icon flaticon-right-arrow-1"></span> saiba mais</a>
</div>
</li>

<li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-3.jpg" data-title="Slide Title" data-transition="parallaxvertical">
    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="{{asset('images/main-slider/image-3.jpg')}}"> 
    <div class="tp-caption" 
    data-paddingbottom="[2,2,1,1]"
    data-paddingleft="[0,0,0,0]"
    data-paddingright="[0,0,0,0]"
    data-paddingtop="[200,200,0,0]"
    data-responsive_offset="on"
    data-type="text"
    data-height="none"
    data-width="['650','700','650','460']"
    data-whitespace="normal"
    data-hoffset="['15','15','15','15']"
    data-voffset="['-80','-90','-70','-80']"
    data-x="['left','left','left','left']"
    data-y="['middle','middle','middle','middle']"
    data-textalign="['top','top','top','top']"
    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
    style="z-index: 7; white-space: nowrap;">
    <h2 style="padding: 200px 0 !important;">Maxsan</h2>
</div>

<div class="tp-caption" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['650','700','650','460']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['80','60','60','40']"
data-x="['left','left','left','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
style="z-index: 7; white-space: nowrap;">
<div style="font-size: 22px !important;" class="text">poder de outro mundo no combate à mosca-branca.</div>
</div>

<div class="tp-caption" 
data-paddingbottom="[0,0,0,0]"
data-paddingleft="[0,0,0,0]"
data-paddingright="[0,0,0,0]"
data-paddingtop="[0,0,0,0]"
data-responsive_offset="on"
data-type="text"
data-height="none"
data-width="['550','550','550','460']"
data-whitespace="normal"
data-hoffset="['15','15','15','15']"
data-voffset="['150','130','130','130']"
data-x="['left','left','left','left']"
data-y="['middle','middle','middle','middle']"
data-textalign="['top','top','top','top']"
data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
style="z-index: 7; white-space: nowrap;">
<a href="/quem-somos" class="know_more"><span class="icon flaticon-right-arrow-1"></span> saiba mais</a>
</div>

</li>

</ul>
</div>
</div>
</section>

<!--CHAMADA -->

<section class="chamada-principal">
    <div class="auto-container">
        <h1 class="text-center chamada-principal-title" style="font-weight: 900;">INOVAÇÃO E QUALIDADE JAPONESAS</h1>
        <h2 class="chamada-principal-title" style="font-size:51px;">A SERVIÇO DA AGRICULTURA BRASILEIRA.</h2>
    </div>
</section>


<!--FIM CHAMADA->



    <!--Market Section-->
    <section class="market-section" style="background-image:url(images/background/2.jpg)">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="title-column m-block col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column" style="min-height: 350px;">
                        <div class="sec-title light">
                            <h2>ENCONTRE A SUA SOLUÇÃO</h2>
                            <div class="separator"></div>
                        </div>
                        <div class="text ">+ de 60 defensivos agrícolas, entre fungicidas,herbicidas, inseticidas e produtos especiais.</div>
                        <a href="/quem-somos" class="view_all"><span class="icon flaticon-right-arrow-1"></span> Ver todos os alvos</a>
                    </div>
                </div>

                <!--Market Block-->
                <div class="market-block m-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box" style="min-height: 350px;">
                        <h3><a href="/home">NEMATOIDES</a></h3>
                        <div class="text hove-text">Veja o tratamento de sementes que controla doenças de solo e nematoides em um só produto</div>
                        <div class="icon-box">
                            <span class="icon"> <img src="{{asset('images/icon-2.png')}}" alt="" srcset=""> </span>
                        </div>
                        <a href="/quem-somos" class="view_all">SAIBA MAIS <span class="icon flaticon-right-arrow-1"></span></a>
                    </div>
                </div>

                <!--Market Block-->
                <div class="market-block m-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box" style="min-height: 350px;">
                        <h3><a href="/home">FERRUGEM ASIÁTICA</a></h3>
                        <div class="text hove-text">Conheça uma molécula inédita para o controle efetivo dessa doença</div>
                        <div class="icon-box">
                            <span class="icon"> <img src="{{asset('images/icon-2.png')}}" alt="" srcset=""> </span>                     
                        </div>
                        <a href="/quem-somos" class="view_all">SAIBA MAIS <span class="icon flaticon-right-arrow-1"></span></a>
                    </div>
                </div>

                <!--Market Block-->
                <div class="market-block m-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box" style="min-height: 350px;">
                        <h3><a href="/home">MOFO BRANCO</a></h3>
                        <div class="text hove-text">Conheça o biológico que combate o mofo branco e a Antracnose</div>
                        <div class="icon-box">
                            <span class="icon"> <img src="{{asset('images/icon-2.png')}}" alt="" srcset=""> </span>                      
                        </div>
                        <a href="/quem-somos" class="view_all">SAIBA MAIS <span class="icon flaticon-right-arrow-1"></span></a>
                    </div>
                </div>

                <!--Market Block-->
                <div class="market-block m-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box" style="min-height: 350px;">
                        <h3><a href="/home">PERCEVEJO</a></h3>
                        <div class="text hove-text">Conheça a tecnologia que elimina de uma vez essa praga</div>
                        <div class="icon-box">
                            <span class="icon"> <img src="{{asset('images/icon-1.png')}}" alt="" srcset=""> </span>                    
                        </div>
                        <a href="/quem-somos" class="view_all">SAIBA MAIS <span class="icon flaticon-right-arrow-1"></span></a>
                    </div>
                </div>

                <!--Market Block-->
                <div class="market-block m-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box" style="min-height: 350px;">
                        <h3><a href="/home">MOSCA BRANCA</a></h3>
                        <div class="text hove-text">Veja uma tecnologia que controla tanto as adultas, quanto as ninfas e ovos.</div>
                        <div class="icon-box">
                            <span class="icon"> <img src="{{asset('images/icon-3.png')}}" alt="" srcset=""> </span>  
                        </div>
                        <a href="/quem-somos" class="view_all">SAIBA MAIS<span class="icon flaticon-right-arrow-1"></span></a>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <!--Últimas Notícias/Projects Section-->
    <section class="projects-section">
        <div class="auto-container">
            <div class="sec-title">
                <div class="row clearfix">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <h2>Últimas Notícias</h2>
                        <div class="separator"></div>
                    </div>
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="text"> Acompanhe as últimas e principais notícias sobre Agricultura. Entrevistas, análises e opinião e muito mais.</div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <a href="/quem-somos" class="view_all">VER TODOS <span class="icon flaticon-right-arrow-1"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">

                @for($i = 0; $i < 3; $i++)

                @if($i == 0)

                <div class="project-block col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box" style="height: 500px;">
                        <div class="image">
                            <img height="600" style="height: 500px;" width="500" src="{{$allnoticias[$i]['imagem']}}" alt="" />
                            <div class="overlay-box">
                                <div class="content">
                                    <h3><a href="{{$allnoticias[$i]['link']}}">{{$allnoticias[$i]['titulo']}}</a></h3>
                                    <div class="text">Lorem iplsuo dolor netab comuns <span class="icon flaticon-right-arrow-1"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endif

                @if($i == 1)
                <div class="project-block col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img style="height: 250px;" src="{{$allnoticias[$i]['imagem']}}" alt="" />
                            <div class="overlay-box">
                                <div class="content">
                                    <h3><a href="{{$allnoticias[$i]['link']}}">{{$allnoticias[$i]['titulo']}}</a></h3>
                                    <div class="text">Lorem iplsuo dolor netab comuns <span class="icon flaticon-right-arrow-1"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($i == 2)
                <div class="project-block col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{$allnoticias[$i]['imagem']}}" style="height: 250px;" alt="" />
                            <div class="overlay-box">
                                <div class="content">
                                    <h3><a href="{{$allnoticias[$i]['link']}}">{{$allnoticias[$i]['titulo']}}</a></h3>
                                    <div class="text">Lorem iplsuo dolor netab comuns<span class="icon flaticon-right-arrow-1"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif


                @endfor


            </div>
            <!--<div class="clearfix">
                
                <div class="project-block col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{asset('images/gallery/project-1.jpg')}}" alt="" />
                            <div class="overlay-box">
                                <div class="content">
                                    <h3><a href="/noticias">Titulo do Artigo</a></h3>
                                    <div class="text">Lorem iplsuo dolor netab comuns <span class="icon flaticon-right-arrow-1"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
                <div class="project-block col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{asset('images/gallery/project-2.jpg')}}" alt="" />
                            <div class="overlay-box">
                                <div class="content">
                                    <h3><a href="/noticias">Titulo do Artigo</a></h3>
                                    <div class="text">Lorem iplsuo dolor netab comuns <span class="icon flaticon-right-arrow-1"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="project-block col-md-6 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image">
                            <img src="{{asset('images/gallery/project-3.jpg')}}" alt="" />
                            <div class="overlay-box">
                                <div class="content">
                                    <h3><a href="/noticias">Titulo do Artigo</a></h3>
                                    <div class="text">Lorem iplsuo dolor netab comuns<span class="icon flaticon-right-arrow-1"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>-->
        </div>
    </section>
    <!--End Últimas Notícias/ Projects Section-->

    <!--Career Section-->
    <section class="career-section">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Video Column-->
                <div class="video-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="image">
                            <img src="{{asset('images/resource/video-img.jpg')}}" alt="" />
                            <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="lightbox-image play-btn">
                                <span class="icon flaticon-play-button-3"></span>
                                ASSISTIR VIDEO
                            </a>
                        </div>
                    </div>
                </div>
                <!--Content Column-->
                <div class="content-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h2>INOVAÇÃO E QUALIDADE JAPONESAS</h2>
                            <div class="separator centered"></div>
                        </div>
                        <div class="text">Há mais de 50 anos, trabalhamos com os agricultores brasileiros para proteger suas lavouras contra pragas, doenças e plantas daninhas. Fazemos isso sob a cultura japonesa da gestão da qualidade, mantendo um time de vendas altamente técnico e rigorosos padrões de produção. É com base nesses princípios que oferecemos mais de 60 defensivos agrícolas, entre fungicidas, herbicidas, inseticidas e produtos especiais.</div>
                        <a class="see_all" href="/noticias">Saiba Mais <span class="icon flaticon-right-arrow-1"></span></a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--End Career Section-->

    <!--Call To Action Section-->
    <!--<section class="call-to-action-section" style="background-image:url(images/background/5.jpg)">
        <div class="auto-container">
            <div class="row clearfix">

                
                <div class="title-column col-md-5 col-sm-12 col-xs-12">
                    <div class="text"><span class="icon flaticon-rss-updates-subscription"></span> Cadastre-se aqui para receber notícias do agronegócio</div>
                </div>
                <div class="subscribe-column col-md-7 col-sm-12 col-xs-12">
                    <div class="subscribe-form">
                        <form method="post" action="contact.html">
                            <div class="form-group">
                                <input class="subscribe-email" type="email" name="email" value="" placeholder="Coloque seu e-mail" required="">
                                <button type="Enviar" class="text-white theme-btn">Inscreva-se <span class="flaticon-right-arrow-1"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <section class="call-to-action-section" style="background-image:url(images/background/5.jpg)">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Title Column-->
                <div class="title-column col-md-5 col-sm-12 col-xs-12">
                    <div class="text" style="padding-left: 54px !important; color:white !important;"><span class="icon flaticon-rss-updates-subscription"></span> Cadastre-se aqui para receber notícias do agronegócio</div>
                </div>
                <!--Subscribe Column-->
                <div class="subscribe-column col-md-7 col-sm-12 col-xs-12">
                    <div id="div-placehold" class="subscribe-form">
                        <form method="post" action="contact.html">
                            <div class="form-group">
                                <input class="subscribe-email" type="email" name="email" value="" placeholder="Coloque seu e-mail" required="">
                                <button type="Enviar" class="text-white theme-btn">Inscreva-se <span class="flaticon-right-arrow-1"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Call To Action Section-->

    <!--Appointment Form-->
    <div class="modal fade" id="schedule-box" tabindex="-1" role="dialog">
        <div class="modal-dialog popup-container container" role="document">
            <div class="modal-content">
                <div class="appoinment_form_wrapper clear_fix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <div class="get-quote-form" style="background-image:url(images/background/13.jpg)">
                        <div class="inner-box">
                            <!--Sec Title-->
                            <div class="sec-title">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h2>Get a Quote</h2>
                                        <div class="separator centered"></div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="text">Get a free quote for your industrial or engineering business solutions, We are here 24/7.</div>
                                    </div>
                                </div>
                            </div>

                            <!-- Quote Form / Style Two-->
                            <div class="quote-form style-two">
                                <!--Shipping Form-->
                                <form method="post" action="contact.html">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="text" placeholder="Your Name" required>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="text" placeholder="Company Name" required>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="text" placeholder="Phone" required>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <select class="custom-select-box">
                                                <option>Select Needed Service</option>
                                                <option>Services One</option>
                                                <option>Services Two</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <textarea placeholder="Your Message..."></textarea>
                                        </div>
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Now <span class="icon flaticon-arrow-pointing-to-right"></span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div> 
            </div> 
        </div> 
        <a href="/home" class="backhome">Voltar pra Home <span class="icon flaticon-arrow-pointing-to-right"></span></a>
    </div>
    <!-- End of #schedule-box -->

    @endsection