@extends('layout')
@section('title','Produtos')
@section('content')

<!--Page Title-->
<section class="page-title" style="background-image:url({{asset('images/background/6.jpg')}})">
    <div class="auto-container">
        <ul class="page-breadcrumb">
            <li><a href="/home">Home</a></li>
            <li>ENCONTRE A SUA SOLUÇÃO</li>
        </ul>
        <h1>ENCONTRE A SUA SOLUÇÃO</h1>
    </div>
</section>
<!--End Page Title-->

<section class="auto-container " id="produtos">
    <form method="get" action="produtos">
        <div class="row mt-5">
            
            <div class="col-3 bg">
                <select class="w-100" id="setor" name="setor">
                    <option id="noneSelect" rel="Selecione um segmento" value="semsegmento">Selecione um segmento</option>
                    <option class="opEmp" value="31" rel="Acaricidas">Acaricidas</option>
                    <option class="opEmp" value="32" rel="Biológicos">Biológicos</option>
                    <option class="opEmp" value="19" rel="Fungicidas">Fungicidas</option>
                    <option class="opEmp" value="17" rel="Herbicidas">Herbicidas</option>
                    <option class="opEmp" value="20" rel="Inseticidas">Inseticidas</option>
                    <option class="opEmp" value="24" rel="Produtos Especiais">Produtos Especiais</option>
                </select>
            </div>
            <div class="col-3 bg">
                <select class="w-100" id="culturas" name="culturas">
                    <option value="">Selecione uma Cultura</option>
                    <option rel="Algodão" value="76">Algodão</option>
                    <option rel="Batata" value="70">Batata</option>
                    <option rel="Berinjela" value="98">Berinjela</option>
                    <option rel="Café" value="118">Café</option>
                    <option rel="Citros" value="66">Citros</option>
                    <option rel="Coco" value="152">Coco</option>
                    <option rel="Crisântemo" value="86">Crisântemo</option>
                    <option rel="Feijão" value="65">Feijão</option>
                    <option rel="Jiló" value="200">Jiló</option>
                    <option rel="Maçã" value="56">Maçã</option>
                    <option rel="Mamão" value="57">Mamão</option>
                    <option rel="Melancia" value="59">Melancia</option>
                    <option rel="Melão" value="60">Melão</option>
                    <option rel="Morango" value="55">Morango</option>
                    <option rel="Pimenta" value="201">Pimenta</option>
                    <option rel="Pimentão" value="97">Pimentão</option>
                    <option rel="Quiabo" value="202">Quiabo</option>
                    <option rel="Rosa" value="90">Rosa</option>
                    <option rel="Soja" value="52">Soja</option>
                    <option rel="Tomate" value="137">Tomate</option>
                    <option rel="Uva" value="48">Uva</option>
                </select>
            </div>
            <div class="col-3 bg">
                <select class="w-100" id="alvos" name="alvos">
                    <option value="">Selecione um Alvo</option>
                    <option rel="Ácaro-branco" value="56">Ácaro-branco (Polyphagotarsonemus latus)</option>
                    <option rel="Ácaro-branco" value="56">Ácaro-branco (Polyphagotarsonemus latus)</option>
                    <option rel="Ácaro-rajado" value="47">Ácaro-rajado (Tetranychus urticae)</option>
                    <option rel="Ácaro-rajado" value="47">Ácaro-rajado (Tetranychus urticae)</option>
                    <option rel="Ácaro-rajado" value="47">Ácaro-rajado (Tetranychus urticae)</option>
                    <option rel="Mosca-branca" value="112">Mosca-branca (Bemisia tabaci)</option>
                </select>
            </div>
            <div class="col-3 bg"><input class="w-100 btn-ihara" type="submit" class="bt-submit" value="BUSCAR"></div>
        </div>
    </form>
</section>


<!--Market Page Section-->
<section class="market-page-section pt-5">
    <div class="auto-container">
        
        <div class="row clearfix">
                
            @for($i = 0; $i < 8; $i++)
            <div class="market-block-four col-md-3 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="lower-content">
                        <a href="/produtos/produto">
                            <img style="padding: 0 20px 40px " src="{{asset('images/background/fusao-logo-icon.png')}}" alt="fusão logo">
                        </a>
                        <h3><a href="/produtos/produto">Fungicidas  Fusão EC </a></h3>
                        <div class="text">Fusão EC é o novo fungicida da IHARA que possui uma molécula...</div>
                        <a class="link-box" href="/produtos/produto">SAIBA MAIS <span class="icon flaticon-arrow-pointing-to-right"></span></a>
                    </div>
                </div>
            </div>
            @endfor

        </div>
            
    </div>
    
</div>
</section>
<!--End Market Section Four-->




<!--Testimonial Section Three-->
<section class="testimonial-section-three" style="background-image:url({{asset('images/background/8.jpg')}})">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>DEPOIMENTOS</h2>
            <div class="separator"></div>
        </div>
        
        <div class="testimonial-item-carousel owl-carousel owl-theme">
            
            <!--Testimonial Block Four-->
            <div class="testimonial-block-four">
                <div class="inner-box">
                    <div class="content-outer">
                        <div class="content-box">
                            <div class="content">
                                <div class="quote-icon">
                                    <span class="icon flaticon-left-quote-2"></span>
                                </div>
                                <div class="text">Must explain to you how mistaken our  denouncing pleasure & praising pain was born we will give expound human happiness pursue pleasure explain to you how mistaken our  denouncing pleasure & we will give expound human happiness pursue pleasure......</div>
                            </div>
                        </div>
                        <div class="layers">
                            <span class="layer-one"></span>
                            <span class="layer-two"></span>
                        </div>
                    </div>
                    <div class="author-info">
                        <div class="image">
                            <img src="{{asset('images/resource/author-4.jpg')}}" alt="" />
                        </div>
                        <h3>Nome sobrenome</h3>
                        <div class="location">Floripa</div>
                    </div>
                </div>
            </div>
            
            <!--Testimonial Block Four-->
            <div class="testimonial-block-four">
                <div class="inner-box">
                    <div class="content-outer">
                        <div class="content-box">
                            <div class="content">
                                <div class="quote-icon">
                                    <span class="icon flaticon-left-quote-2"></span>
                                </div>
                                <div class="text">Must explain to you how mistaken our  denouncing pleasure & praising pain was born we will give expound human happiness pursue pleasure explain to you how mistaken our  denouncing pleasure & we will give expound human happiness pursue pleasure......</div>
                            </div>
                        </div>
                        <div class="layers">
                            <span class="layer-one"></span>
                            <span class="layer-two"></span>
                        </div>
                    </div>
                    <div class="author-info">
                        <div class="image">
                            <img src="{{asset('images/resource/author-4.jpg')}}" alt="" />
                        </div>
                        <h3>Nome sobrenome</h3>
                        <div class="location">Floripa</div>
                    </div>
                </div>
            </div>
            
            <!--Testimonial Block Four-->
            <div class="testimonial-block-four">
                <div class="inner-box">
                    <div class="content-outer">
                        <div class="content-box">
                            <div class="content">
                                <div class="quote-icon">
                                    <span class="icon flaticon-left-quote-2"></span>
                                </div>
                                <div class="text">Must explain to you how mistaken our  denouncing pleasure & praising pain was born we will give expound human happiness pursue pleasure explain to you how mistaken our  denouncing pleasure & we will give expound human happiness pursue pleasure......</div>
                            </div>
                        </div>
                        <div class="layers">
                            <span class="layer-one"></span>
                            <span class="layer-two"></span>
                        </div>
                    </div>
                    <div class="author-info">
                        <div class="image">
                            <img src="{{asset('images/resource/author-4.jpg')}}" alt="" />
                        </div>
                        <h3>Nome sobrenome</h3>
                        <div class="location">Floripa</div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</section>
<!--End Testimonial Section Three-->




<!--Call To Action Section-->
<section class="call-to-action-section" style="background-image:url({{asset('images/background/5.jpg')}})">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <h2>Contribuindo para a competitividade da agricultura brasileira.</h2>
            </div>
            <div class="link-column col-md-3 col-sm-12 col-xs-12">
                <a class="apointment" href="/quem-somos">Saiba Mais <span class="icon flaticon-arrow-pointing-to-right"></span></a>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action Section-->

<!--Appointment Form-->
<div class="modal fade" id="schedule-box" tabindex="-1" role="dialog">
    <div class="modal-dialog popup-container container" role="document">
        <div class="modal-content">
            <div class="appoinment_form_wrapper clear_fix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="get-quote-form" style="background-image:url({{asset('images/background/13.jpg')}})">
                    <div class="inner-box">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2>Get a Quote</h2>
                                    <div class="separator centered"></div>
                                </div>
                                <div class="pull-left">
                                    <div class="text">Get a free quote for your industrial or engineering business solutions, We are here 24/7.</div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Quote Form / Style Two-->
                        <div class="quote-form style-two">
                            <!--Shipping Form-->
                            <form method="post" action="contact.html">
                                <div class="row clearfix">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="text" placeholder="Your Name" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="text" placeholder="Company Name" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="text" placeholder="Phone" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <select class="custom-select-box">
                                            <option>Select Needed Service</option>
                                            <option>Services One</option>
                                            <option>Services Two</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <textarea placeholder="Your Message..."></textarea>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Now <span class="icon flaticon-arrow-pointing-to-right"></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
    <a href="/home" class="backhome">Voltar pra Home <span class="icon flaticon-arrow-pointing-to-right"></span></a>
</div>
<!-- End of #schedule-box -->

@endsection