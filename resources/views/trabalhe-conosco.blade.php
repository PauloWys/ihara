<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>IHARA - Agricultura é a nossa vida</title>
    <!-- Stylesheets -->
    <link href="{{asset('css/style2.css')}}" rel="stylesheet">
    <!--<link href="css/color.css" rel="stylesheet">-->

    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<body>

    <header class="bg-ihara pt-3 pb-3">
        <div class="container d-flex flex-row justify-content-between">

            <div class="logo"><a href="/home"><img src="{{asset('images/logo.png')}}" alt=""></a></div>
            <div class="d-flex flex-row justify-content-between align-self-center">
                <div><a href="/home" class="text-white">Home</a></div> 
                <div class="text-white pr-3 pl-3"><p> &#9679;</p> </div>
                <div class="text-white">Trabalhe Conosco</div>
            </div>

        </div>
    </header>

    <section class="passos">
        <div class="container">
            <div class="row text-center">
                <div class=" m-0 p-3 col-md-6 passos_aba ativo " data-id="primeiro">
                    <a class="etapaUm text-ihara" href="#"><div class="p-3 border-ihara">Primeiro Passo</div></a>
                </div>
                <div class=" m-0 p-3 col-md-6 passos_aba " data-id="segundo">
                    <a class="etapaDois text-ihara" href="#"><div class="p-3 border-ihara">Segundo Passo</div></a>
                </div>
            </div>
        </div>
    </section>

    <form id="trabalheConosco" enctype="multipart/form-data">

        {{-- <input class="w-100" type="hidden" name="_token" value="IeMnKvufFvM70uuCJaFowrABRzG2VvcvFQt5Sdqz"> --}}
        
        <div class="container">
            {{-- <div class="row"> --}}

                {{-- init primeiro --}}
                <div class="passo primeiro" id="primeiro">
                    <div class="row">

                        <div class="col-md-4 col-sm-12 hidden-xs">
                            <div class="padding-ten-all bg-light-gray border-radius-6 md-padding-seven-all xs-padding-30px-all sm-text-center">
                                <span class="text-large font-weight-bold alt-font text-extra-dark-gray margin-5px-bottom display-block">
                                    Passo 1
                                </span>
                                <p>
                                    Obrigado por se interessar em fazer parte do time Ihara. <br>
                                    Para iniciar o processo de candidatura, preencha seus dados abaixo.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="padding-seven-all bg-light-gray border-radius-6">
                                <span class="text-medium text-justify font-weight-500 alt-font text-extra-dark-gray margin-5px-bottom display-block">
                                    <p>Pariatur do cillum ut.</p>
                                    <p>Lorem ipsum culpa voluptate non ea dolore nostrud cupidatat officia qui nostrud proident incididunt in in aliqua.</p>
                                </span>
                                <p>Campos marcados com <span class="required text-danger font-weight-bold">*</span> <strong>são obrigatórios</strong></p>
                                <div class="row formInput">
                                    <div class="mb-3 col-md-8">
                                        <label for="nome">Nome:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="nome" id="nome" placeholder="Nome Completo">
                                    </div>
                                    <div class="mb-3 col-md-4">
                                        <label for="data_nascimento">Data de Nascimento:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="data_nascimento" id="data_nascimento" placeholder="Ex.: 99/99/9999">
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="email">E-mail:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="mail" name="email" id="email" placeholder="Seu E-mail">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="telefone">Telefone:</label>
                                        <input class="w-100" type="text" name="telefone" id="telefone" placeholder="Ex.: (99) 9999-9999">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="celular">Celular:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="celular" id="celular" placeholder="Ex.: (99) 9 9999-9999">
                                    </div>
                                    <div class="mb-3 col-md-7">
                                        <label for="indicacao">Você foi indicado por alguém do time Ihara? Quem?<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="indicacao" id="indicacao" placeholder="Nome de quem indicou">
                                    </div>
                                    <div class="mb-3 col-md-5">
                                        <label for="locomocao">Meio de locomoção até a Ihara?<span class="required text-danger font-weight-bold">*</span></label>
                                        <select class="w-100" name="locomocao" id="locomocao">
                                            <option value="">-- Selecione --</option>
                                            <option value="Veículo Próprio">Veículo Próprio</option>
                                            <option value="Transporte Público">Transporte Público</option>
                                            <option value="Carona">Carona</option>
                                            <option value="Fretado">Fretado</option>
                                        </select>
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="facebook">Link do Facebook:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="facebook" id="facebook" placeholder="Ex.: https://www.facebook.com/Ihara">
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="linkedin">Link do Linkedin:</label>
                                        <input class="w-100" type="text" name="linkedin" id="linkedin" placeholder="Ex.: https://br.linkedin.com/company/Ihara">
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="instagram">Link do Instagram:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="instagram" id="instagram" placeholder="Ex.: https://www.instagram.com/Ihara/">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="endereco">Endereço:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="endereco" id="endereco" placeholder="Ex.: Nome da Rua, número">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="cidade">Cidade:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="cidade" id="cidade" placeholder="Ex.: Cidade">
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="estado">Estado:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="estado" id="estado" placeholder="Ex.: Estado">
                                    </div>
                                    <div class="mb-3 col-md-3">
                                        <label for="pais">País:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="pais" id="pais" placeholder="Ex.: Brasil">
                                    </div>
                                    <div class="mb-3 col-md-3">
                                        <label for="cep">CEP:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="cep" id="cep" placeholder="Ex.: 00000-000" maxlength="9">
                                    </div>
                                    <div class="mb-3 col-md-8">
                                        <label for="cargo">Cargo:<span class="required text-danger font-weight-bold">*</span></label>
                                        <select class="w-100" name="cargo" id="cargo">
                                            <option value="Cargo 1"> Cargo 1 </option>
                                            <option value="Cargo 2"> Cargo 2 </option>
                                            <option value="Cargo 3"> Cargo 3 </option>
                                            <option value="Cargo 4"> Cargo 4 </option>
                                            <option value="Cargo 5"> Cargo 5 </option>

                                        </select>
                                    </div>
                                    <div class="mb-3 col-md-4">
                                        <label for="cargo">Nivel:<span class="required text-danger font-weight-bold">*</span></label>
                                        <select class="w-100" id="nivel" name="nivel">
                                            <option value="Estágio">
                                                Estágio
                                            </option>
                                            <option value="Junior">
                                                Junior
                                            </option>
                                            <option value="Pleno">
                                                Pleno
                                            </option>
                                            <option value="Senior">
                                                Senior
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <a href="#" class="m-0 btn btn-ihara p-3 btn_etapaDois">
                                            Próxima etapa <i class="fa fa-long-arrow-right"></i>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {{-- end primeiro --}}
                
                <div class="clearfix"></div>
                
                {{-- init segundo --}}
                <div class="passo segundo" id="segundo">
                    <div class="row">

                        <div class="mb-3 col-md-4 col-sm-12 hidden-xs">
                            <div class="padding-ten-all bg-light-gray border-radius-6 md-padding-seven-all xs-padding-30px-all sm-text-center">
                                <span class="text-large font-weight-bold alt-font text-extra-dark-gray margin-5px-bottom display-block">
                                    Passo 2
                                </span>
                                <p>
                                    Obrigado por se interessar em fazer parte do time Ihara. <br>
                                    Para iniciar o processo de candidatura, preencha seus dados abaixo.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="padding-seven-all bg-light-gray border-radius-6">
                                <span class="text-medium text-justify font-weight-500 alt-font text-extra-dark-gray margin-5px-bottom display-block">
                                    <p>Lorem ipsum pariatur proident do.</p>
                                    <p>Duis esse aute sint quis tempor occaecat irure dolor consectetur in in ea cillum sit ad in sed ea.</p>
                                </span>
                                <p>Campos marcados com <span class="required text-danger font-weight-bold">*</span> <strong>são obrigatórios</strong></p>
                                <div class="row formInput">
                                    <div class="mb-3 col-md-6">
                                        <label for="experiencia">Já possúi experiência?<span class="required text-danger font-weight-bold">*</span></label>
                                        <select class="w-100" name="experiencia" id="experiencia">
                                            <option value="">-- Selecione --</option>
                                            <option value="Sim">Sim</option>
                                            <option value="Não">Não</option>
                                        </select>
                                    </div>
                                    <div class="mb-3 col-md-6">
                                        <label for="salario">Expectativa Salarial:<span class="required text-danger font-weight-bold">*</span></label>
                                        <input class="w-100" type="text" name="salario" id="salario" placeholder="R$">
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="portfolio">Link do portfólio:</label>
                                        <input class="w-100" type="text" name="portfolio" id="portfolio" placeholder="https://www.seusite.com.br">
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="mensagem">Conte-nos um pouco sobre você:<span class="required text-danger font-weight-bold">*</span></label>
                                        <textarea class="w-100" name="mensagem" id="mensagem" placeholder="Apresente-se para nós!" rows="5"></textarea>
                                    </div>
                                    <div class="mb-3 col-md-12">
                                        <label for="cv">Anexar Currículo:<span class="required text-danger font-weight-bold">*</span> (somente pdf)</label>
                                        <input class="w-100" type="file" name="cv" id="cv" accept="application/pdf">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <a href="#" class="m-0 btn btn-ihara p-3 btn_etapaUm">
                                            <i class="fa fa-long-arrow-left"></i> Voltar etapa
                                        </a>
                                        <button class="btn btn-ihara p-3" id="trabalhe">
                                            Enviar <i class="fa fa-send"></i>
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {{-- end segundo --}}
                <div class="clearfix"></div>
            {{-- </div> --}}
        </div>

    </form>

    <footer class="mt-5 pt-5 pt-0">
        <div class="container d-flex flex-column justify-content-between small font-weight-bold">
            <div class="d-flex flex-row justify-content-start">
                <div class="pr-5"><a class="text-dark" href="/home">HOME</a></div>
                <div class="pr-5"><a class="text-dark" href="/quem-somos">QUEM SOMOS</a></div>
                <div class="pr-5"><a class="text-dark" href="/produtos">PRODUTOS</a></div>
                <div class="pr-5"><a class="text-dark" href="/noticias">NOTÍCIAS</a></div>
                <div class="pr-5"><a class="text-dark" href="/contato">CONTATO </a></div>
            </div>

            <div class="small pt-2 pb-2">TODOS OS DIREITOS RESERVADOS © 2019 | IHARA</div>

            <div class="small">AV. LIBERDADE, 1701 CAJURU DO SUL SOROCABA - SP CEP: 18087-170</div>
            {{-- +55 (15) 3235-7700  CONTATO@IHARA.COM.BR --}}


        </div>
    </footer>

    <script>
    // alert('1');
    
    var etapaUm =  document.querySelector('.etapaUm');
    var etapaDois =  document.querySelector('.etapaDois');
    var btn_etapaUm =  document.querySelector('.btn_etapaUm');
    var btn_etapaDois =  document.querySelector('.btn_etapaDois');
    
    var primeiro =  document.querySelector('#primeiro');
    var segundo =  document.querySelector('#segundo');
    var primeiroFocus =  document.querySelector('#nome');
    var segundoFocus =  document.querySelector('#experiencia');

    etapaUm.addEventListener('click', function(e){
        e.preventDefault();
        primeiro.style.display = "block";
        segundo.style.display = "none";
        primeiroFocus.focus();

    });
    etapaDois.addEventListener('click', function(e){ 
        e.preventDefault();
        primeiro.style.display = "none";
        segundo.style.display = "block";
        segundoFocus.focus();
    });
    
    btn_etapaUm.addEventListener('click', function(e){ 
        e.preventDefault();
        primeiro.style.display = "block";
        segundo.style.display = "none";
        primeiroFocus.focus();
    });
    btn_etapaDois.addEventListener('click', function(e){ 
        e.preventDefault();
        primeiro.style.display = "none";
        segundo.style.display = "block";
        segundoFocus.focus();
    });
    
    segundo.style.display = "none";

</script>

<script src="{{asset('js/jquery.js')}}"></script> 
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

</body>
</html>