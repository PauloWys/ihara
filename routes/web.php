<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                           'PagesController@index')->name('index.home');

Route::get('/imprensa',                    function () {return view('home'); })->name('home');
Route::get('/trabalhe-conosco',            function () {return view('home'); })->name('home');

Route::get('/ptbr',                        function () {return view('home'); })->name('home');
Route::get('/en',                          function () {return view('home'); })->name('home');

Route::get('/area-cliente',                function () {return view('home'); })->name('home');
Route::get('/area-cliente',                function () {return view('home'); })->name('home');
Route::get('/area-colaborador',            function () {return view('home'); })->name('home');
Route::get('/filtro-produtos',            function () {return view('filtro'); })->name('filtro');

Route::get('/home',                        function () {return view('home'); })->name('home');
Route::get('/culturas',                    function () {return view('home'); })->name('home');
Route::get('/imprensa',                    function () {return view('imprensa'); })->name('imprensa');
Route::get('/noticias',                    function () {return view('noticias'); })->name('noticias');
Route::get('/produto',                    function () {return view('produto'); })->name('produto');
Route::get('/produtos',                    function () {return view('produtos'); })->name('produtos');
Route::get('/politica-privacidade',                    function () {return view('politica-privacidade'); })->name('politica');

Route::get('/produtos/produto-interno/{id}',         'ProdutoController@index')->name('produtos-internos');
Route::get('/cidades-estado/{id}',         'ProdutoController@allcidades')->name('cidades');

Route::get('/quem-somos',                  function () {return view('quem-somos'); })->name('quem-somos');
Route::get('/sustentabilidade',            function () {return view('home'); })->name('home');
Route::get('/trabalhe-conosco',            function () {return view('trabalhe-conosco'); })->name('trabalhe-conosco');
Route::get('/contato',                     function () {return view('contato'); })->name('contato');
Route::get('/contato2',                     function () {return view('contato2'); })->name('contato2');
Route::get('/sustentabilidade',                     function () {return view('sustentabilidade'); })->name('sustentabilidade');
Route::get('/produtos-min-sem-logo',                     function () {return view('ihara-produto-min-sem-logo'); })->name('ihara-produto-min-sem-logo');


Route::get('/facebook',                    function () {return view('home'); })->name('home');
Route::get('/instagram',                   function () {return view('home'); })->name('home');
Route::get('/linkedin',                    function () {return view('home'); })->name('home');	
Route::get('/twitter',                     function () {return view('home'); })->name('home');