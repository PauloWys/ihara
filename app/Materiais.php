<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materiais extends Model
{
    protected $table = 'materiais';

	
	protected $fillable = [
		'nome',
		'arquivo'
	];
}
