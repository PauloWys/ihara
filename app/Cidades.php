<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidades extends Model
{
	public $timestamps = false;

	protected $table = 'cidades';

	
	protected $fillable = [
		'estados_cod_estados',
		'cod_cidades',
		'nome',
		'cep'
	];
}
