<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alvo extends Model
{
	public $timestamps = false;

	protected $table = 'alvo';

	
	protected $fillable = [
		'nome',
		'texto',
		'img'
		
	];
}
