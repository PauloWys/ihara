<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
	public $timestamps = false;

	protected $table = 'produtos';

	
	protected $fillable = [
		'nome',
		'banner',
		'thumb',
		'slogan',
		'text_descri',
		'img_benef1',
		'texto_benef1',
		'img_benef2',
		'texto_benef2',
		'img_benef3',
		'texto_benef3',
		'img_benef4',
		'texto_benef4',
		'img_confira',
	];
    //
}
