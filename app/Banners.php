<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
	public $timestamps = false;

	protected $table = 'banners';

	
	protected $fillable = [
		'page_id',
		'textoh1',
		'textoh3',
		'h1slide2',
		'h3slide2',
		'h1slide3',
		'h3slide3',
		'imghead',
		'imgfix',
		'imgfix2'
	];
    //
}
