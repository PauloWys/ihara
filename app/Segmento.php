<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segmento extends Model
{
	protected $table = 'segmento';

	
	protected $fillable = [
		'nome',
		'cor'
	];
}
