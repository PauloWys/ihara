<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Materiais;
use App\Segmento;
use App\Estado;
use App\Cidades;

class ProdutoController extends Controller
{
	public function index($id)
	{
		$estados = Estado::All();

		$produto = Produto::findOrFail($id);
         //dd($produto);
		$id_seg = Produto::where('id', $id)->select('id_segmento')->get();

		$segmento = Segmento::findOrFail($id_seg);
		$materiais = Materiais::where('produto_id', $id)->get();
		$cor = $segmento[0]['cor'];
		$nome = $segmento[0]['nome'];

         //dd($segmento);

		return view('produtos-internos', compact('produto', 'materiais', 'cor', 'nome', 'estados'));

	}

	public function allcidades($cod){


		$citys = Cidades::where('estados_cod_estados', $cod)->get();

		foreach ($citys as $cidade) {
			
			$cidades[] = array(

				'cod_cidades'	=> $cidade['cod_cidades'],
				'nome'			=> $cidade['nome']

			);

		}
        return response()->json(['cidades'=>$cidades]);
		//dd($cidades);
	}
}
