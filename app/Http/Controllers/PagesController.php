<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PagesRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

use App\Paginas;
use App\Banners;
use App\Alvo;
//use App\Categoria;
//use App\Prods;
//use App\ProdutosImagens;
//use App\Newsletter;

use App\Mail\Contato;




class PagesController extends Controller
{
    public function index(Request $request)
    {     
        $banners = Banners::findOrFail(1);
        $alvos = Alvo::where('id_pagina', 1)->get();
        $pagina = Paginas::findOrFail(1);

        $ch = curl_init("http://iharablog.servidorwys.com.br/wp-json/wp/v2/posts?_embed");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $noticias =  json_decode(curl_exec($ch));
        //dd($noticias[0]->link);

        foreach ($noticias as $noticia) {

            $allnoticias[] = array(
                'titulo' => $noticia->title->rendered,
                'imagem' => $noticia->_embedded->{'wp:featuredmedia'}[0]->media_details->sizes->full->source_url,
                'texto' => $noticia->content->rendered,
                'link'  => $noticia->link
            );
        }
        
       //print_r($allnoticias);
       //exit();


        return view('home', compact( 'banners', 'alvos','pagina','allnoticias'));
        
    }

    public function edit(Request $request, $id)
    {

        /* 1 = Administrador | 2 = Cliente | 3 = Conteudoria | 4 = Editor | 5 = Redator */
        /*if(!$request->user()->authorizeRoles(['1','2','3','4'])) {
            $title = "Acesso não autorizado";
            return view('cms.errors.401', compact('title'));
        }
        
        $page = Page::findOrFail($id);
        $title = 'Editando: '.$page->title;*/


        $page = Paginas::findOrFail($id);
        $banners = Banners::findOrFail($id);
        //dd($banners);
        //dd($page);
        

        return view('painel.paginas.index', compact( 'page' ,'banners'));
    }

    public function update(Request $request, $id)
    {
        //dd($id);
        $banner = new Banners();

        if($request->hasfile('imghead')){

            $file = $request->file('imghead');
            $file_name = $file->getClientOriginalName();
            $file_path = 'img/';
            //dd($file_path);

            $file->move($file_path, $file_name);

            $banner->where('page_id', 1)->update(['imghead' => $file_name]);

            //print_r($file_name);

            
        }


        if($request->hasfile('imgfix')){

            $file = $request->file('imgfix');
            $file_name = $file->getClientOriginalName();
            $file_path = 'img/';
            //dd($file_path);

            $file->move($file_path, $file_name);

            $banner->where('page_id', 1)->update(['imgfix' => $file_name]);

            //print_r($file_name);

            
        }


        if($request->hasfile('imgfix2')){

            $file = $request->file('imgfix2');
            $file_name = $file->getClientOriginalName();
            $file_path = 'img/';
            //dd($file_path);

            $file->move($file_path, $file_name);

            $banner->where('page_id', 1)->update(['imgfix2' => $file_name]);

            
        }

        $banner->where('page_id', $id)->update(['textoh1' => $request['textoh1'],

            'textoh3' => $request['textoh3'],
            'h1slide2' => $request['textoh1-slide2'],
            'h3slide2' => $request['textoh3-slide2'],
            'h1slide3' => $request['textoh1-slide3'],
            'h3slide3' => $request['textoh3-slide3']]);


        Session::flash('message', 'Editado com sucesso!');
        Session::flash('class', 'success');
        //dd('AKI');
        return redirect()->route('pages.edit', $id);
    }

    //Views
    public function home()
    {
        $page = Page::findOrFail(1);

        $title = $page->name;
        return view('home', compact('title', 'page'));
    }

    public function contato()
    {
        $page = Page::findOrFail(3);

        $title = $page->name;
        return view('contato', compact('title', 'page'));
    }

    public function produto($categoria, $id){

        $page = Page::findOrFail(4);

        $produto = Prods::where('id', $id )->first();

        $title = $page->name;
        return view('quemsomos', compact('title', 'page'));

    }

    public function produtos($slug = null){

        $page = Page::findOrFail(4);

        if ($slug !== null) {
            $categ = Categoria::where('slug', $slug)->firstOrFail(['id']);
            $produtos = Prods::where('categoria_id', $categ->id)->orderBy('id', 'DESC')->paginate(8);
            $title = $categ->label;
        } else {
            $produtos = Prods::orderBy('id', 'DESC')->paginate(8);
            $title = $page->name;
        }

        $categorias = Categoria::all();

        return view('produtos', compact('title', 'page', 'produtos', 'categorias'));

    }

    public function enviarcontato(Request $request)
    {
        $submit = $request->all();
        $sendto = 'site@ventilare.com.br';

        /* Adicionar dados do cadastro em array */
        $data = [
            'nome'=> $submit['nome'],
            'email'=> $submit['email'],
            'celular'=> $submit['celular'],
            'mensagem'=> $submit['mensagem']
        ];

        /* Enviar e-mail */
        Mail::to($sendto)->send(new Contato($data));
    }

    public function enviarnewsletter(Request $request)
    {
        $submit = $request->all();

        Newsletter::create($submit);
    }
}